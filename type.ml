type t =
    | Unit
    | Bool
    | Int
    | Float
    | Fun of t list * t
    | Tuple of t list
    | Array of t
    | Var of t option ref
    | Main

(* generate a new type *)
let gentyp () = Var(ref None)

let print = function
    | Unit -> Printf.fprintf stdout "Unit"
    | Bool -> Printf.fprintf stdout "Bool"
    | Int -> Printf.fprintf stdout "Int"
    | Float -> Printf.fprintf stdout "Float"
    | Fun(_) -> Printf.fprintf stdout "Fun(_)"
    | Tuple(_) -> Printf.fprintf stdout "Tuple(_)"
    | Array(_) -> Printf.fprintf stdout "Array(_)"
    | Var(_) -> Printf.fprintf stdout "Var(_)"
    | Main -> Printf.fprintf stdout "Main"
