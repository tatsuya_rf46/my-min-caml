open Ir

let hInstr bs in_out (i, j) = function
    | Let((x, t), Mov(y)) ->
            let reach = ConstPropagate.reach bs in_out (i, j) y in
            if List.length reach != 1 then
                Let((x, t), Mov(y))
            else
                let (i', j') = List.hd reach in
                (match ConstPropagate.nthExp bs (i', j') with
                | Let(_, Mov(y')) when is_reg y' -> Let((x, t), Mov(y)) (* callee save better not propageted *)
                | Let((x', t'), exp) when y = x' ->
                        let (ivars, fvars) = exp_vars' exp in
                        let exp_vars = S.union ivars fvars in
                        (* check if vars in exp will be overwrited *)
                        if S.exists
                            (fun var ->
                                let reach = List.filter
                                    (fun r -> Stdlib.compare (i, j) r > 0)
                                    (ConstPropagate.reach' bs in_out (i, j) var) in
                                (* (1* debug *1) if y = "Ti1517.4800" then *)
                                (*     (Printf.fprintf stderr "ok...@"; *)
                                (*     Printf.fprintf stderr "i, j: %d, %d\n" i j; *)
                                (*     Printf.fprintf stderr "%d\n" (S.cardinal exp_vars); *)
                                (*     S.iter (fun s -> Printf.fprintf stderr "%s \n" s) exp_vars; *)
                                (*     List.iter (fun (i, j) -> Printf.fprintf stderr "(%d, %d)\n" i j) reach) *)
                                (*     else (); *)
                                List.exists
                                    (fun r ->
                                        List.length (ConstPropagate.reach' bs in_out r y) > 0)
                                    reach)
                            exp_vars then
                                Let((x, t), Mov(y))
                            else
                                Let((x, t), exp)
                | _ -> assert false)
    | instr -> instr
let rec h' bs in_out (i, j) = function
    | [] -> []
    | e :: rem -> hInstr bs in_out (i, j) e :: h' bs in_out (i, j+1) rem
let rec h bs in_out i = function
    | [] -> []
    | b :: rem -> h' bs in_out (i, 0) b :: h bs in_out (i+1) rem

let g { name = Id.L(s); args = ys; fargs = zs; Block.body_block = bs; ret = t } =
    (* Printf.printf "func: %s\n" s; *)
    let in_out = ReachDef.f bs in
    (* ReachDef.print in_out; *)
    let bs = h bs in_out 0 bs in
    { name = Id.L(s); args = ys; fargs = zs; Block.body_block = bs; ret = t }

let f (data, fundefs, e) =
    let in_out = ReachDef.f e in
    (data, List.map (fun fundef -> g fundef) fundefs, h e in_out 0 e)
