open Asm

type dependence = RAW | WAR | WAW | JUMP

module InstrS =
    Set.Make
        (struct
            type t = int (* nth instr *)
            let compare = Stdlib.compare
        end)
module InstrM =
    Map.Make
        (struct
            type t = int (* nth instr *)
            let compare = Stdlib.compare
        end)

type graph = {
    mutable adjList : InstrS.t InstrM.t
}
let graph = {
    adjList = InstrM.empty
}
let initGraph b =
    graph.adjList <- InstrM.empty;
    List.iteri
        (fun i _ -> graph.adjList <- InstrM.add i InstrS.empty graph.adjList)
        b
let printGraph () =
    InstrM.iter
        (fun i s ->
            InstrS.iter
                (fun j -> Printf.printf "(%d, %d) " i j)
                s)
        graph.adjList

let addEdge u v =
    graph.adjList <- InstrM.add u (InstrS.add v (InstrM.find u graph.adjList)) graph.adjList

let readVars = function
    | Label(_) -> assert false
    | Instr("jal", _, [arg_num; farg_num], None, [_])
    | Instr("j", _, [arg_num; farg_num], None, [_]) ->
            let arg_num, farg_num = int_of_string arg_num, int_of_string farg_num in
            (List.init arg_num (fun i -> Ir.caller_regs.(i))) @
                (List.init farg_num (fun i -> Ir.caller_fregs.(i)))
    | Instr(_, _, src, _, _) -> src
let writeVars = function
    | Label(_) -> assert false
    | Instr("sw", None, [_; y], Some(_), [])
    | Instr("sw.s", None, [_; y], Some(_), []) -> Some(y)
    | Instr(_, dst, _, _, _) -> dst

(* i1 depends on i0 *)
let dependent i0 i1 =
    match i0, i1 with
    | Label(_), _ -> Some(JUMP)
    | _, Label(_) -> Some(JUMP)
    | Instr(mne, _, _, _, _), _ when mne = "fork" || mne = "next" || mne = "end" -> Some(JUMP)
    | _, Instr(mne, _, _, _, _) when mne = "fork" || mne = "next" || mne = "end" -> Some(JUMP)
    (* TODO: removet this case by constant propagation *)
    | Instr("sw", None, [_; y], Some(_), []), _ when y != Ir.reg_sp -> Some(JUMP)
    (* TODO: removet this case by constant propagation *)
    | _, Instr("sw", None, [_; y], Some(_), []) when y != Ir.reg_sp -> Some(JUMP)
    | Instr("addi", Some(reg_sp0), [reg_sp1], Some(_), []), _
        when reg_sp0 = Ir.reg_sp && reg_sp1 = Ir.reg_sp -> Some(JUMP)
    | _, Instr("addi", Some(reg_sp0), [reg_sp1], Some(_), [])
        when reg_sp0 = Ir.reg_sp && reg_sp1 = Ir.reg_sp -> Some(JUMP)
    | _, Instr("jr", None, [_; _], None, [])
    | _, Instr("j", None, _, None, [_])
    | _, Instr("beq", None, [_; _], None, [_; _])
    | _, Instr("bne", None, [_; _], None, [_; _])
    | _, Instr("bf.s", None, [], Some(0), [_; _]) -> Some(JUMP)
    | _, _ ->
            let r0, w0 = readVars i0, writeVars i0 in
            let r1, w1 = readVars i1, writeVars i1 in
            match w0 with
            | Some(w0) ->
                    if List.mem w0 r1 then Some(RAW)
                    else
                        (match w1 with
                        | Some(w1) ->
                                if List.mem w1 r0 then Some(WAR)
                                else if w0 = w1 then Some(WAW) else None
                        | None -> None)
            | None ->
                    (match w1 with
                    | Some(w1) ->
                            if List.mem w1 r0 then Some(WAR) else None
                    | None -> None)

(* build graph *)
let rec build nth = function
    | [] -> ()
    | i :: rem ->
            List.iteri
                (fun k i' ->
                    match dependent i i' with
                    | Some(_) -> addEdge nth (nth + k + 1)
                    | _ -> ())
                rem;
            build (nth+1) rem

let readySet () =
    InstrM.fold
        (fun i _ ret ->
            if InstrM.for_all (fun _ s -> not (InstrS.mem i s)) graph.adjList then
                i :: ret
            else
                ret)
        graph.adjList
        []

let rec allocate b =
    if InstrM.is_empty graph.adjList then
        []
    else
        let ready_set = readySet () in
        (* let i = List.nth ready_set ((List.length ready_set) - 1) in (1* TODO: consider preference *1) *)
        (* let i = List.hd ready_set in *)
        (* graph.adjList <- InstrM.remove i graph.adjList; *)
        (* (List.nth b i) :: allocate b *)
        let is' = List.map (fun i -> List.nth b i) (List.rev ready_set) in
        List.iter
            (fun i -> graph.adjList <- InstrM.remove i graph.adjList)
            ready_set;
        is' @ allocate b

let schedule b =
    (* (1* debug *1) Printf.printf "initGraph\n"; *)
    (*     printBlock stdout 0 b; *)
    initGraph b;
    build 0 b;
    (* (1* debug *1) printGraph (); *)
    (*     Printf.printf "\n"; *)
    allocate b

let rec h = function
    | [] -> []
    | b :: rem ->
            let b' = schedule b in
            b' :: h rem

let g { name = Id.L(s); args = ys; fargs = zs; body_block = bs; ret = t } =
    { name = Id.L(s); args = ys; fargs = zs; body_block = h bs; ret = t }

let f (data, funblocks, bs) =
    Format.eprintf "scheduling...@.";
    let funblocks = List.map g funblocks in
    let bs = h bs in
    (data, funblocks, bs)
