open Ir

module BlockS =
    Set.Make
        (struct
            type t = int (* nth block *)
            let compare = Stdlib.compare
        end)
module BlockM =
    Map.Make
        (struct
            type t = int (* nth block *)
            let compare = Stdlib.compare
        end)

type graph = {
    mutable adjList : BlockS.t BlockM.t
}
let graph = {
    adjList = BlockM.empty
}
let initGraph bs =
    graph.adjList <- BlockM.empty;
    List.iteri
        (fun i _ -> graph.adjList <- BlockM.add i BlockS.empty graph.adjList)
        bs

let addEdge u v =
    graph.adjList <- BlockM.add u (BlockS.add v (BlockM.find u graph.adjList)) graph.adjList

let pred u =
    BlockM.fold
        (fun i s rem ->
            if BlockS.mem u s then BlockS.add i rem else rem)
        graph.adjList
        BlockS.empty

let createIrCtlgraph bs =
    List.iteri
        (fun i b0 ->
            List.iteri
            (fun j b1 ->
                if List.mem (Block.get_label b1) (Block.succ_labels b0)
                then addEdge i j
                else ())
            bs)
        bs

let createAsmCtlgraph bs =
    List.iteri
        (fun i b0 ->
            List.iteri
            (fun j b1 ->
                if List.mem (Asm.getLabel b1) (Asm.succLabels b0)
                then addEdge i j
                else ())
            bs)
        bs
