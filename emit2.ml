open Asm

external getf : float -> int32 = "getf"

let emitSSS chan mne r0 r1 r2 =
    Printf.fprintf chan "%4s%-8s%s, %s, %s\n" "" mne r0 r1 r2
let emitSSI chan mne r0 r1 i =
    Printf.fprintf chan "%4s%-8s%s, %s, %d\n" "" mne r0 r1 i
let emitSIS chan mne r0 i r1 =
    Printf.fprintf chan "%4s%-8s%s, %d(%s)\n" "" mne r0 i r1
let emitISS chan mne i s0 s1 =
    Printf.fprintf chan "%4s%-8s%d, %s, %s\n" "" mne i s0 s1
let emitSS chan mne s0 s1 =
    Printf.fprintf chan "%4s%-8s%s, %s\n" "" mne s0 s1
let emitIS chan mne i s =
    Printf.fprintf chan "%4s%-8s%d, %s\n" "" mne i s
let emitSI chan mne s i =
    Printf.fprintf chan "%4s%-8s%s, %d\n" "" mne s i
let emitS chan mne l =
    Printf.fprintf chan "%4s%-8s%s\n" "" mne l
let emit chan mne =
    Printf.fprintf chan "%4s%-8s\n" "" mne
let emitLabel chan l =
    Printf.fprintf chan "%s:\n" l

let h oc = function
    | Label(Id.L(s)) -> emitLabel oc s
    | Instr("nop", Some(x), [], None, []) -> ()
    | Instr("add", Some(x), [y; v], None, []) -> emitSSS oc "add" x y v
    | Instr("addi", Some(x), [y], Some(c), []) -> emitSSI oc "addi" x y c
    | Instr("sub", Some(x), [y; z], None, []) -> emitSSS oc "sub" x y z
    | Instr("sll", Some(x), [y], Some(2), []) -> emitSSI oc "sll" x y 2 (* "<< 2" *)
    | Instr("srl", Some(x), [y], Some(1), []) -> emitSSI oc "srl" x y 1 (* "> 1" *)
    | Instr("sll", Some(x), [y], Some(c), []) -> emitSSI oc "sll" x y c
    | Instr("srl", Some(x), [y], Some(c), []) -> emitSSI oc "srl" x y c
    | Instr("lw", Some(x), [y], Some(c), []) -> emitSIS oc "lw" x c y
    | Instr("li", Some(x), [], Some(c), []) -> emitSI oc "li" x c
    | Instr("move", Some(x), [y], None, []) -> emitSS oc "move" x y
    | Instr("la", Some(x), [], None, [Id.L(s)]) -> emitSS oc "la" x s
    | Instr("sw", None, [x; y], Some(c), []) -> emitSIS oc "sw" x c y
    | Instr("lw.s", Some(x), [y], Some(c), []) -> emitSIS oc "lw.s" x c y
    | Instr("lw.s", Some(x), [], None, [Id.L(s)]) -> emitSS oc "lw.s" x s
    | Instr("sw.s", None, [x; y], Some(c), []) -> emitSIS oc "sw.s" x c y
    | Instr("add.s", Some(x), [y; z], None, []) -> emitSSS oc "add.s" x y z
    | Instr("sub.s", Some(x), [y; z], None, []) -> emitSSS oc "sub.s" x y z
    | Instr("mul.s", Some(x), [y; z], None, []) -> emitSSS oc "mul.s" x y z
    | Instr("div.s", Some(x), [y; z], None, []) -> emitSSS oc "div.s" x y z
    | Instr("mov.s", Some(x), [y], None, []) -> emitSS oc "mov.s" x y
    | Instr("neg.s", Some(x), [y], None, []) -> emitSS oc "neg.s" x y
    | Instr("ftoi", Some(x), [y], None, []) -> emitSS oc "ftoi" x y
    | Instr("itof", Some(x), [y], None, []) -> emitSS oc "itof" x y
    | Instr("abs.s", Some(x), [y], None, []) -> emitSS oc "abs.s" x y
    | Instr("sqrt.s", Some(x), [y], None, []) -> emitSS oc "sqrt.s" x y
    | Instr("cadd", None, [x; y], None, []) -> emitSS oc "cadd" x y
    | Instr("jal", _, [_; _], None, [Id.L(s)]) -> emitS oc "jal" s
    | Instr("jr", None, [_; reg_ra], None, []) -> emitS oc "jr" reg_ra
    | Instr("j", None, [_; _], None, [Id.L(s)]) -> emitS oc "j" s
    | Instr("beq", None, [x; y], None, [_; Id.L(s)]) -> emitSSS oc "beq" x y s
    | Instr("bne", None, [x; y], None, [_; Id.L(s)]) -> emitSSS oc "bne" x y s
    | Instr("slt", Some(x), [y; z], None, []) -> emitSSS oc "slt" x y z
    | Instr("slti", Some(x), [y], Some(c), []) -> emitSSI oc "slti" x y c
    | Instr("c.eq.s", None, [x; y], Some(0), []) -> emitSSS oc "c.eq.s" "0" x y
    | Instr("c.lt.s", None, [x; y], Some(0), []) -> emitSSS oc "c.lt.s" "0" x y
    | Instr("bf.s", None, [], Some(0), [_; Id.L(s)]) -> emitSS oc "bf.s" "0" s
    | Instr("fork", None, [x], Some(c), []) -> emitSI oc "fork" x c
    | Instr("next", Some(x), [y], None, []) when x = y -> emitS oc "next" x
    | Instr("end", None, [], None, []) -> emit oc "end"
    | Instr(mne, _, _, _, _) -> Printf.printf "%s\n" mne; assert false

let rec g oc = function
    | [] -> ()
    | i :: rem ->
            h oc i;
            g oc rem

(* let rec g oc = function *)
(*     | [] -> () *)
(*     | b :: rem -> *)
(*             g' oc b; *)
(*             g oc rem *)

let f oc (data, funcodes, is) =
    Format.eprintf "generating assembly...@.";
    List.iter
        (fun (Id.L(x), s) ->
            Printf.fprintf oc "%s:    # %f\n" x s;
            Printf.fprintf oc "    0x%lx\n" (getf s);)
        data;
    Printf.fprintf oc "    .text\n";
    Printf.fprintf oc "    .globl    min_caml_start\n";
    List.iter (fun is -> g oc is) funcodes;
    g oc is
