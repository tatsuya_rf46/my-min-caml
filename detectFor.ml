open Ir

let rec detectForInternal' x s = function
    | [] -> assert false
    | Ans(_) :: rem -> false
    | Let((x', _), exp) :: rem when x = x' ->
            let (ivars, fvars) = exp_vars' exp in
            if S.equal (S.union ivars fvars) (S.singleton x) then
                detectForInternal' x s rem
            else
                false
    | Let(_) :: rem ->
            detectForInternal' x s rem
    | Label(_) :: rem -> assert false
    | Jump(Id.L(s')) :: rem -> if (s ^ "_start") = s' then true else detectForInternal x s (Id.L(s')) rem
    | IfEq(_, _, l0, l1) :: rem | IfLT(_, _, l0, l1) :: rem
    | IfFEq(_, _, l0, l1) :: rem | IfFLT(_, _, l0, l1) :: rem ->
            detectForInternal x s l0 rem && detectForInternal x s l1 rem
    | For(_) :: rem -> assert false
and detectForInternal x s l = function
    | [] -> assert false
    | Label(l') :: rem when l = l' -> detectForInternal' x s rem
    | e :: rem -> detectForInternal x s l rem
let rec detectForExternal' s = function
    | [] -> assert false
    | Ans(_) :: rem -> true
    | Let(_) :: rem -> detectForExternal' s rem
    | Label(_) :: rem -> assert false
    | Jump(Id.L(s')) :: rem -> if s = s' then false else true
    | IfEq(_, _, l0, l1) :: rem | IfLT(_, _, l0, l1) :: rem
    | IfFEq(_, _, l0, l1) :: rem | IfFLT(_, _, l0, l1) :: rem ->
            detectForExternal s l0 rem && detectForExternal s l1 rem
    | For(_) :: rem -> assert false
and detectForExternal s l = function
    | [] -> assert false
    | Label(l') :: rem when l = l' -> detectForExternal' s rem
    | e :: rem -> detectForExternal s l rem
let rec detectFor s ys = function
    | Ans(_) :: rem -> false
    | Let(_) :: rem | Label(_) :: rem ->
            detectFor s ys rem
    | Jump(_) :: rem -> detectFor s ys rem
    | IfLT(x, C(c), l0, l1) :: rem ->
            if List.mem x ys then
                detectForExternal s l0 rem && detectForInternal x s l1 rem
            else
                detectFor s ys rem
    | _ -> false

let rec hForInternal x = function
    | [] -> assert false
    | Let((x', _), _) as step :: rem when x = x' ->
            step, rem
    | e :: rem ->
            let step, rem' = hForInternal x rem in
            step, e :: rem'
let rec h' x l = function
    | [] -> assert false
    | Label(l') as e :: rem when l = l' ->
            let step, rem' = hForInternal x rem in
            step, e :: rem'
    | e :: rem ->
            let step, rem' = h' x l rem in
            step, e :: rem'
let rec h is_parallel = function
    | Let(_) as instr :: rem ->
            instr :: h is_parallel rem
    | Jump(_) as instr :: rem ->
            instr :: h is_parallel rem
    | Label(base) :: IfLT(x, C(c), l0, l1) :: rem ->
            let step, rem' = h' x l1 rem in
            if is_parallel then
                Label(base) :: For(base, x, c, step, l0, l1, true) :: rem'
            else
                Label(base) :: For(base, x, c, step, l0, l1, false) :: rem'
    | Label(_) as instr :: rem ->
            instr :: h is_parallel rem
    | _ -> assert false

let g parallel_func { name = Id.L(s); args = ys; fargs = zs; Block.body_block = bs; ret = t } =
    let es = List.flatten bs in
    let bs =
        if detectFor s ys es then
            (Format.eprintf "%s - for sentese detected!@." s;
            (* check parallelization *)
            let es' =
                if String.length s < String.length parallel_func then
                    h false es
                else
                    (if (String.sub s 0 (String.length parallel_func)) = parallel_func then
                        (Format.eprintf "%s will be parallelized...@." parallel_func;
                        h true es) 
                    else
                    h false es)
            in
            Block.h es')
        else
            bs in
    { name = Id.L(s); args = ys; fargs = zs; Block.body_block = bs; ret = t }

let f (data, fundefs, e) =
    Format.eprintf "which function to be parallelized? ('no' - no parallelize)@.";
    Format.eprintf "-   input here@.";
    let parallel_func = Scanf.bscanf Scanf.Scanning.stdin "%s" (fun s -> s) in
    (data, List.map (fun fundef -> g parallel_func fundef) fundefs, e)
