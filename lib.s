min_caml_create_array:
    move    $a2, $a0
    nop
    nop
    0
    move    $a0, $gp
    nop
    nop
    0
create_array_loop:
    nop
    nop
    beq     $a2, $zero, create_array_return
    4
    sw      $a1, 0($gp)
    addi    $a2, $a2, -1
    nop
    0
    addi    $gp, $gp, 4
    nop
    j       create_array_loop
    4
create_array_return:
    nop
    nop
    jr      $ra
    4
min_caml_create_float_array:
    move    $a2, $a0
    nop
    nop
    0
    move    $a0, $gp
    nop
    nop
    0
create_float_array_loop:
    nop
    nop
    beq     $a2, $zero, create_array_return
    4
    sw.s    $f0, 0($gp)
    addi    $a2, $a2, -1
    nop
    0
    addi    $gp, $gp, 4
    nop
    j       create_float_array_loop
    4
create_array_return:
    nop
    nop
    jr      $ra
    4
min_caml_print_char:
    out     $a0
    nop
    nop
    0
    nop
    nop
    jr      $ra
    4
min_caml_read_float:
    lui     $a0,   0
    nop
    nop
    0
    in      $a0
    nop
    nop
    0
    lui     $a1,   0
    nop
    nop
    0
    in      $a1
    nop
    nop
    0
    sll     $a1, $a1, 8
    nop
    nop
    0
    or      $a0, $a0, $a1
    nop
    nop
    0
    lui     $a1,   0
    nop
    nop
    0
    in      $a1
    nop
    nop
    0
    sll     $a1, $a1, 16
    nop
    nop
    0
    or      $a0, $a0, $a1
    nop
    nop
    0
    lui     $a1,   0
    nop
    nop
    0
    in      $a1
    nop
    nop
    0
    sll     $a1, $a1, 24
    nop
    nop
    0
    or      $a0, $a0, $a1
    nop
    nop
    0
    sw      $a0, 0($sp)
    nop
    nop
    0
    lw.s    $f0, 0($sp)
    nop
    nop
    0
    nop
    nop
    jr      $ra
    4
min_caml_read_int:
    lui     $a0,   0
    nop
    nop
    0
    in      $a0
    nop
    nop
    0
    lui     $a1,   0
    nop
    nop
    0
    in      $a1
    nop
    nop
    0
    sll     $a1, $a1, 8
    nop
    nop
    0
    or      $a0, $a0, $a1
    nop
    nop
    0
    lui     $a1,   0
    nop
    nop
    0
    in      $a1
    nop
    nop
    0
    sll     $a1, $a1, 16
    nop
    nop
    0
    or      $a0, $a0, $a1
    nop
    nop
    0
    lui     $a1,   0
    nop
    nop
    0
    in      $a1
    nop
    nop
    0
    sll     $a1, $a1, 24
    nop
    nop
    0
    or      $a0, $a0, $a1
    nop
    nop
    0
    nop
    nop
    jr      $ra
    4
min_caml_print_newline:
    nop
    nop
    jr      $ra
    4
min_caml_truncate:
    nop
    ftoi    $a0, $f0
    jr      $ra
    7
