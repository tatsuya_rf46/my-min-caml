open Ir

type fundef = { name : Id.l; args : Id.t list; fargs : Id.t list; body_block : t list list; ret : Type.t }

let rec h = function (* start a block *)
    | [] -> []
    | Label(l) :: rem as es -> h' es
    | _ :: rem -> h rem (* ignore except a label *)
and h' = function
    | [] -> []
    | Ans(exp) :: rem -> (* finish the current trace and start a new one *)
            [Ans(exp)] :: h rem
    | Jump(l) :: rem ->
            [Jump(l)] :: h rem
    | IfEq(x, y, l1, l2) :: rem ->
            [IfEq(x, y, l1, l2)] :: h rem
    | IfLT(x, y, l1, l2) :: rem ->
            [IfLT(x, y, l1, l2)] :: h rem
    | IfFEq(x, y, l1, l2) :: rem ->
            [IfFEq(x, y, l1, l2)] :: h rem
    | IfFLT(x, y, l1, l2) :: rem ->
            [IfFLT(x, y, l1, l2)] :: h rem
    | For(_) as instr :: rem ->
            [instr] :: h rem
    | e :: rem ->
            match h' rem with
            | [] -> [e :: [Ans(Nop)]]
            | e' :: rem' -> (e :: e') :: rem'

let g { name = l; args = ys; fargs = zs; body = es; ret = t } =
    { name = l; args = ys; fargs = zs; body_block = h es; ret = t }

let f (data, fundefs, es) =
    let fundefs = List.map g fundefs in
    let bs =  h es in
    (data, fundefs, bs)

let get_label = function (* return the block's label *)
    | Label(l) :: _ -> l
    | _ -> assert false

let rec succ_labels = function (* return labels of successors *)
    | [] -> assert false
    | [Ans(_)] -> []
    | [Jump(l)] -> [l]
    | [IfEq(_, _, l1, l2)] -> [l1; l2]
    | [IfLT(_, _, l1, l2)] -> [l1; l2]
    | [IfFEq(_, _, l1, l2)] -> [l1; l2]
    | [IfFLT(_, _, l1, l2)] -> [l1; l2]
    | [For(_, _, _, _, l0, l1, _)] -> [l0; l1]
    | _ :: rem -> succ_labels rem

let print_block chan depth b =
    List.iter
        (fun e -> print_t chan depth e)
        b;
    Printf.fprintf chan "\n"
let print_fundef chan { name = Id.L(x); args = _; fargs = _; body_block = bs; ret = _ } =
    Printf.fprintf chan "%s:\n" x;
    List.iter
        (fun b -> print_block chan 1 b)
        bs
let print chan (data, fundefs, bs) =
    List.iter
        (fun fundef -> print_fundef chan fundef)
        fundefs;
    Printf.fprintf chan "Main\n";
    List.iter
        (fun b -> print_block chan 1 b)
        bs;
    (data, fundefs, bs)
