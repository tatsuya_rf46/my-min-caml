#include <stdio.h>
#include <stdint.h>
#include <caml/mlvalues.h>
#include <caml/alloc.h>

typedef union {
    int32_t i;
    float s;
} sgl;

value getf(value v) {
    sgl s;
    // 63bit floating point in OCaml
    s.s = (float) Double_val(v);
    return copy_int32(s.i);
}
