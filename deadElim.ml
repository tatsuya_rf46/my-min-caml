open Ir

let rec h' bs in_out (i, j) = function
    | [] -> []
    | Let(_, Call(_)) as e :: rem -> (* call may contain side effects *)
            e :: h' bs in_out (i, j+1) rem
    | Let((x, t), exp) :: rem ->
            let rem' = h' bs in_out (i, j+1) rem in
            if not_reg x && t != Type.Unit && List.length (ConstPropagate.use bs in_out (i, j)) = 0 then
                rem'
            else
                Let((x, t), exp) :: rem'
    | e :: rem -> e :: h' bs in_out (i, j+1) rem
let rec h bs in_out i = function
    | [] -> []
    | b :: rem -> h' bs in_out (i, 0) b :: h bs in_out (i+1) rem

let g { name = Id.L(s); args = ys; fargs = zs; Block.body_block = bs; ret = t } =
    (* Printf.printf "func: %s\n" s; *)
    let in_out = ReachDef.f bs in
    (* ReachDef.print in_out; *)
    let bs = h bs in_out 0 bs in
    { name = Id.L(s); args = ys; fargs = zs; Block.body_block = bs; ret = t }

let f (data, fundefs, e) =
    let in_out = ReachDef.f e in
    (data, List.map (fun fundef -> g fundef) fundefs, h e in_out 0 e)
