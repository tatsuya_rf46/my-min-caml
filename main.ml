let limit = ref 0
let lib = ref None
let glb = ref None

let rec iter n prog =
    Format.eprintf "iteration %d@." n;
    if n = 0 then prog else
    let prog' = (DeadElim.f (CopyPropagate.f (ConstPropagate.f (Inline.f prog)))) in
    if prog = prog' then prog else
    iter (n-1) prog'

let rec kIter n prog =
    Format.eprintf "iteration %d@." n;
    if n = 0 then prog else
    let prog' = KInline.f prog in
    if prog = prog' then prog else
    kIter (n - 1) prog'

let lexbuf dbgchan synchan knchan apchan virchan lpchan blkchan optchan asmchan schchan regchan outchan l =
    Id.counter := 0;
    Typing.extenv := M.empty;
    (* Emit2.f outchan *)
    Emit.f outchan              (* emit assembly *)
    (Vliw.f
    (RegAlloc.print regchan
    (RegAlloc.f
    (Asm.print schchan
    (Schedule.f
    (Asm.print asmchan
    (Asm.f
    (Block.print dbgchan
    (* (iter !limit *)
    (Block.print optchan
    (DetectFor.f
    (DeadElim.f
    (CopyPropagate.f
    (ConstPropagate.f
    (Block.print blkchan
    (Block.f
    (Ir.print lpchan            (* debug *)
    (Loop.f
    (Ir.print virchan           (* debug *)
    (Virtual.f                  (* virtual code *)
    (KNormal.print apchan       (* debug *)
    (kIter !limit
    (Alpha.f                    (* alpha conversion *)
    (KNormal.print knchan       (* debug *)
    (KNormal.f                  (* k-normalization *)
    (Typing.f                   (* type inference *)
    (Syntax.print synchan       (* debug *)
    (Yacc.top Lex.token l       (* lexing and parsing *)
    )))))))))))))))))))))))))))

let file f =
    (* input file *)
    let inchan = open_in (f ^ ".ml") in
    (* debug file *)
    let dbgchan = open_out (f ^ "_dbg.txt") in
    (* syntax file *)
    let synchan = open_out (f ^ "_syntax.txt") in
    (* kNormal file *)
    let knchan = open_out (f ^ "_kNormal.txt") in
    (* alpha file *)
    let apchan = open_out (f ^ "_alpha.txt") in
    (* virtual file *)
    let virchan = open_out (f ^ "_vir.txt") in
    (* loop file *)
    let lpchan = open_out (f ^ "_loop.txt") in
    (* block file *)
    let blkchan = open_out (f ^ "_blk.txt") in
    (* optimized file *)
    let optchan = open_out (f ^ "_opt.txt") in
    (* asm file *)
    let asmchan = open_out (f ^ "_asm.txt") in
    (* schedule file *)
    let schchan = open_out (f ^ "_sch.txt") in
    (* reg file *)
    let regchan = open_out (f ^ "_reg.txt") in
    (* output file *)
    let outchan = open_out (f ^ ".s") in
    let rec append_file chan1 chan2 =
        output_char chan2 (input_char chan1);
        append_file chan1 chan2
    in
    match !lib, !glb with
    | None, None ->
        (try
            lexbuf dbgchan synchan knchan apchan virchan lpchan blkchan optchan asmchan schchan regchan outchan (Lexing.from_channel inchan);
            close_in inchan;
            close_out outchan;
         with e -> (close_in inchan; close_out outchan; raise e))
    | Some(_), Some(_) ->
        (try
            (* library file *)
            let libchan = open_in (Option.get !lib) in
            (* object file *)
            let obchan_out = open_out (f ^ ".mlo") in
            try
                append_file libchan obchan_out
            with End_of_file -> close_in libchan;
            try
                append_file inchan obchan_out
            with End_of_file -> (close_in inchan; close_out obchan_out);
            (* reopen object file *)
            let obchan_in = open_in (f ^ ".mlo") in
            let glbchan = open_in (Option.get !glb) in
            try
                append_file glbchan outchan
            with End_of_file -> close_in glbchan;
            lexbuf dbgchan synchan knchan apchan virchan lpchan blkchan optchan asmchan schchan regchan outchan (Lexing.from_channel obchan_in);
            close_in inchan;
            close_out outchan;
            close_in libchan;
            close_in glbchan;
            close_out obchan_out;
            close_in obchan_in;
         with e -> (close_in inchan; close_out outchan; raise e))
    | None, Some(_) ->
        (try
            let glbchan = open_in (Option.get !glb) in
            try
                append_file glbchan outchan
            with End_of_file -> close_in glbchan;
            lexbuf dbgchan synchan knchan apchan virchan lpchan blkchan optchan asmchan schchan regchan outchan (Lexing.from_channel inchan);
            close_in inchan;
            close_out outchan;
            close_in glbchan;
         with e -> (close_in inchan; close_out outchan; raise e))
    | _, _ -> ()

let () =
    let files = ref [] in
    Arg.parse
        [("-iter", Arg.Int(fun i -> limit := i), "maximum number of optimizations iterated");
         ("-l", Arg.String(fun s -> lib := Some(s)), "library file path");
         ("-g", Arg.String(fun s -> glb := Some(s)), "global file path")]
        (fun s -> files := !files @ [s])
        ("My-Min-Caml Compiler");
    List.iter
        (fun f -> ignore (file f))
        !files
