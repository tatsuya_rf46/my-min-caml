%{
    open Syntax
    let addtyp x = (x, Type.gentyp ())
    let getpos () =
        let pos = Parsing.symbol_start_pos () in
        let lnum = pos.Lexing.pos_lnum in
        let cnum = pos.Lexing.pos_cnum - pos.Lexing.pos_bol in
        { line = lnum; chars = cnum }
%}

%token <bool> BOOL
%token <int> INT
%token <float> FLOAT
%token NOT
%token MINUS
%token PLUS
%token AST
%token SLASH
%token MINUS_DOT
%token PLUS_DOT
%token AST_DOT
%token SLASH_DOT
%token EQUAL
%token LESS_GREATER
%token LESS_EQUAL
%token GREATER_EQUAL
%token LESS
%token GREATER
%token IF
%token THEN
%token ELSE
%token <Id.t> IDENT
%token WILDCARD
%token LET
%token IN
%token REC
%token COMMA
%token ARRAY_CREATE
%token DOT
%token LESS_MINUS
%token SEMICOLON
%token LPAREN
%token RPAREN
%token EOF

%nonassoc IN
%right prec_let
%right SEMICOLON
%right prec_if
%right LESS_MINUS
%nonassoc prec_tuple
%left COMMA
%left EQUAL LESS_GREATER LESS GREATER LESS_EQUAL GREATER_EQUAL
%left PLUS MINUS PLUS_DOT MINUS_DOT
%left AST SLASH AST_DOT SLASH_DOT
%right prec_unary_minus
%left prec_app
%left DOT

%type <Syntax.fundef list * Syntax.t> top
%type <Syntax.t> exp
%start top

%%

top:
    | LET REC fundef IN top
        %prec prec_let
        { (fun f (fs, e) -> (f :: fs, e)) $3 $5 }
    | LET WILDCARD EQUAL exp IN INT
        %prec prec_let
        { ([], $4) }

fundef:
    | IDENT formal_args EQUAL exp
        { { name = (addtyp $1, getpos ()); args = $2; body = $4 } }

exp: /* (* normal expressions *) */
    | simple_exp
        { $1 }
    | NOT exp
        %prec prec_app
        { Not($2) }
    | MINUS exp
        %prec prec_unary_minus
        { match $2 with
        | Float(f, _) -> Float(-.f, getpos ())
        | e -> Neg(e) }
    | exp PLUS exp
        { Add($1, $3) }
    | exp MINUS exp
        { Sub($1, $3) }
    | exp AST exp
        { Mul($1) }
    | exp SLASH exp
        { Div($1) }
    | exp EQUAL exp
        { Eq($1, $3) }
    | exp LESS_GREATER exp
        { Not(Eq($1, $3)) }
    | exp LESS exp
        { LT($1, $3) }
    | exp GREATER exp
        { LT($3, $1) }
    | exp LESS_EQUAL exp
        { Not(LT($3, $1)) }
    | exp GREATER_EQUAL exp
        { Not(LT($1, $3)) }
    | IF exp THEN exp ELSE exp
        %prec prec_if
        { If($2, $4, $6) }
    | MINUS_DOT exp
        %prec prec_unary_minus
        { FNeg($2) }
    | exp PLUS_DOT exp
        { FAdd($1, $3) }
    | exp MINUS_DOT exp
        { FSub($1, $3) }
    | exp AST_DOT exp
        { FMul($1, $3) }
    | exp SLASH_DOT exp
        { FDiv($1, $3) }
    | LET IDENT EQUAL exp IN exp
        %prec prec_let
        { Let(addtyp $2, $4, $6) }
    | simple_exp actual_args
        %prec prec_app
        { App($1, $2) }
    | elems
        %prec prec_tuple
        { Tuple($1) }
    | LET LPAREN pat RPAREN EQUAL exp IN exp
        { LetTuple($3, $6, $8) }
    | simple_exp DOT LPAREN exp RPAREN LESS_MINUS exp
        {
            Put($1, $4, $7)
        }
    | exp SEMICOLON exp
        { Let((Id.gentmp Type.Unit, Type.Unit), $1, $3) }
    | exp SEMICOLON
        { $1 }
    | ARRAY_CREATE simple_exp simple_exp
        %prec prec_app
        { Array($2, $3) }
    | error
        { failwith
            (Printf.sprintf "parse error near line:%d charcters %d-%d"
                ((Parsing.symbol_start_pos ()).pos_lnum)
                ((Parsing.symbol_start_pos ()).Lexing.pos_cnum - (Parsing.symbol_start_pos ()).Lexing.pos_bol)
                ((Parsing.symbol_end_pos ()).Lexing.pos_cnum - (Parsing.symbol_end_pos ()).Lexing.pos_bol))}

simple_exp: /* (* expressions that can be a argument without parenthesis *) */
    | LPAREN exp RPAREN
        { $2 }
    | LPAREN RPAREN
        { Unit(getpos ()) }
    | BOOL
        { Bool($1, getpos ()) }
    | INT
        { Int($1, getpos ()) }
    | FLOAT
        { Float($1, getpos ()) }
    | IDENT
        { Var($1, getpos ()) }
    | simple_exp DOT LPAREN exp RPAREN
        { Get($1, $4) }

formal_args:
    | IDENT formal_args
        { addtyp $1 :: $2 }
    | WILDCARD formal_args
        { addtyp (Id.gentmp Type.Unit) :: $2 }
    | IDENT
        { [addtyp $1] }
    | WILDCARD
        { [addtyp (Id.gentmp Type.Unit)] }

actual_args:
    | actual_args simple_exp
        %prec prec_app
        { $1 @ [$2] }
    | simple_exp
        %prec prec_app
        { [$1] }

elems:
    | elems COMMA exp
        { $1 @ [$3] }
    | exp COMMA exp
        { [$1; $3] }

pat:
    | pat COMMA IDENT
        { $1 @ [addtyp $3] }
    | IDENT COMMA IDENT
        { [addtyp $1; addtyp $3] }
