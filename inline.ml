open Ir

let find env x = try M.find x env with Not_found -> x

let alphaExp env = function
    | Nop -> Nop
    | Add(x, V(y)) -> Add(find env x, V(find env y))
    | Add(x, C(c)) -> Add(find env x, C(c))
    | Sub(x, y) -> Sub(find env x, find env y)
    | Mul(x) -> Mul(find env x)
    | Div(x) -> Div(find env x)
    | SLL(x, c) -> SLL(find env x, c)
    | SRL(x, c) -> SRL(find env x, c)
    | Lw(c, x) -> Lw(c, find env x)
    | Li(c) -> Li(c)
    | Mov(x) -> Mov(find env x)
    | La(l) -> La(l)
    | Sw(x, c, y) -> Sw(find env x, c, find env y)
    | FLw(c, V(x)) -> FLw(c, V(find env x))
    | FLw(c, L(l)) -> FLw(c, L(l))
    | FSw(x, c, y) -> FSw(find env x, c, find env y)
    | FAdd(x, y) -> FAdd(find env x, find env y)
    | FSub(x, y) -> FSub(find env x, find env y)
    | FMul(x, y) -> FMul(find env x, find env y)
    | FDiv(x, y) -> FDiv(find env x, find env y)
    | FMov(x) -> FMov(find env x)
    | FNeg(x) -> FNeg(find env x)
    | Ftoi(x) -> Ftoi(find env x)
    | Itof(x) -> Itof(find env x)
    | FAbs(x) -> FAbs(find env x)
    | FSqrt(x) -> FSqrt(find env x)
    | CAdd(x, y) -> CAdd(find env x, find env y)
    | Call(l, ys, zs) -> Call(l, List.map (fun y -> find env y) ys, List.map (fun z -> find env z) zs)

let rec alpha' env = function
    | [] -> []
    | Ans(exp) :: rem ->
            Ans(alphaExp env exp) :: alpha' env rem
    | Let((x, t), exp) :: rem ->
            let x', env' =
                (try M.find x env, env with Not_found ->
                    if is_reg x then
                        x, env
                    else
                        let x' = Id.genid x in
                        x', M.add x x' env) in
            Let((x', t), alphaExp env exp) :: alpha' env' rem
    | Label(Id.L(s)) :: rem -> Label(Id.L(M.find s env)) :: alpha' env rem
    | Jump(Id.L(s)) :: rem -> Jump(Id.L(find env s)) :: alpha' env rem
    | IfEq(x, y, Id.L(s0), Id.L(s1)) :: rem ->
            IfEq(find env x, find env y, Id.L(M.find s0 env), Id.L(M.find s1 env)) :: alpha' env rem
    | IfLT(x, V(y), Id.L(s0), Id.L(s1)) :: rem ->
            IfLT(find env x, V(find env y), Id.L(M.find s0 env), Id.L(M.find s1 env)) :: alpha' env rem
    | IfLT(x, C(c), Id.L(s0), Id.L(s1)) :: rem ->
            IfLT(find env x, C(c), Id.L(M.find s0 env), Id.L(M.find s1 env)) :: alpha' env rem
    | IfFEq(x, y, Id.L(s0), Id.L(s1)) :: rem ->
            IfFEq(find env x, find env y, Id.L(M.find s0 env), Id.L(M.find s1 env)) :: alpha' env rem
    | IfFLT(x, y, Id.L(s0), Id.L(s1)) :: rem ->
            IfFLT(find env x, find env y, Id.L(M.find s0 env), Id.L(M.find s1 env)) :: alpha' env rem
    | For(Id.L(s0), x, c, e, Id.L(s1), Id.L(s2), isParallel) :: rem ->
            let e' = List.hd (alpha' env [e]) in
            For(Id.L(M.find s0 env), find env x, c, e', Id.L(M.find s1 env), Id.L(M.find s2 env), isParallel) :: alpha' env rem

let rec alphaLabel env = function
    | [] -> env
    | Label(Id.L(s)) :: rem ->
            let s' = Id.genid s in
            alphaLabel (M.add s s' env) rem
    | e :: rem -> alphaLabel env rem

let alpha env es =
    let env' = alphaLabel env es in
    alpha' env' es

let rec findFun l = function
    | [] -> None
    | { name = l'; args = _; fargs = _; Block.body_block = _; ret = _ } as fundef :: rem when l = l' ->
            Some(fundef)
    | _ :: rem -> findFun l rem

let inlinedEs l ys zs { name = _; args = ys'; fargs = zs'; Block.body_block = bs; ret = _ } =
        let env =
            List.fold_left2
                (fun env y' y -> M.add y' y env)
                (List.fold_left2
                    (fun env z' z -> M.add z' z env)
                    M.empty
                    zs'
                    zs)
                ys'
                ys in
        alpha env (List.flatten bs)

let rec isLoop' s = function
    | [] -> false
    | b :: rem ->
            if Label(Id.L(s^"_start")) = List.nth b (List.length b - 1) then
                true
            else isLoop' s rem
let isLoop { name = Id.L(s); args = _; fargs = _; Block.body_block = bs; ret = _ } =
    let hd = List.hd bs in
    assert (Jump(Id.L(s^"_start")) = List.nth hd (List.length hd - 1));
    assert (Label(Id.L(s^"_start")) = List.hd (List.nth bs 1));
    isLoop' s (List.tl bs)

let rec h t fundefs = function
    | [] -> []
    | Ans(Call(l, ys, zs)) as e :: rem ->
            (match findFun l fundefs with
            | None -> e :: h t fundefs rem
            | Some(fundef) ->
                    inlinedEs l ys zs fundef @ h t fundefs rem)
    | Let(xt, Call(l, ys, zs)) as e :: rem ->
            (match findFun l (List.filter (fun fundef -> isLoop fundef) fundefs) with
            | None -> e :: h t fundefs rem
            | Some(fundef) -> bind xt (inlinedEs l ys zs fundef) @ h t fundefs rem)
    | e :: rem -> e :: h t fundefs rem

let g fundefs { name = Id.L(s); args = ys; fargs = zs; Block.body_block = bs; ret = t } =
    let bs' = Block.h (h t fundefs (List.flatten bs)) in
    { name = Id.L(s); args = ys; fargs = zs; Block.body_block = bs'; ret = t }

let f (data, fundefs, bs) =
    let bs' = Block.h (h Type.Unit fundefs (List.flatten bs)) in
    (data, List.map (fun fundef -> g fundefs fundef) fundefs, bs')
