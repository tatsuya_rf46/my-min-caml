type id_or_l = V of Id.t | L of Id.l
type id_or_imm = V of Id.t | C of int

type t =
    | Ans of exp
    | Let of (Id.t * Type.t) * exp
    | Label of Id.l
    | Jump of Id.l
    | IfEq of Id.t * Id.t * Id.l * Id.l
    | IfLT of Id.t * id_or_imm * Id.l * Id.l
    | IfFEq of Id.t * Id.t * Id.l * Id.l
    | IfFLT of Id.t * Id.t * Id.l * Id.l
    | For of Id.l * Id.t * int * t * Id.l * Id.l * bool (* l0: for(; x>=c; t) {l2;} l1; (is parallel) *)
and exp =
    | Nop
    | Add of Id.t * id_or_imm
    | Sub of Id.t * Id.t
    | Mul of Id.t
    | Div of Id.t
    | SLL of Id.t * int
    | SRL of Id.t * int
    | Lw of int * Id.t
    | Li of int
    | Mov of Id.t
    | La of Id.l
    | Sw of Id.t * int * Id.t
    | FLw of int * id_or_l
    | FSw of Id.t * int * Id.t
    | FAdd of Id.t * Id.t
    | FSub of Id.t * Id.t
    | FMul of Id.t * Id.t
    | FDiv of Id.t * Id.t
    | FMov of Id.t
    | FNeg of Id.t
    | Ftoi of Id.t
    | Itof of Id.t
    | FAbs of Id.t
    | FSqrt of Id.t
    | CAdd of Id.t * Id.t (* y := !y .+ x *)
    | Call of Id.l * Id.t list * Id.t list

type fundef = { name : Id.l; args : Id.t list; fargs : Id.t list; body : t list; ret : Type.t }

let caller_regs =
    [| "$a0"; "$a1"; "$a2"; "$a3"; "$t0"; "$t1"; "$t2"; "$t3";
       "$t4"; "$t5"; "$t6"; "$t7"; "$t8"; "$t9"; "$k0"; "$k1";
       "$v0"; "$v1" |]
let callee_regs =
    [| "$s0"; "$s1"; "$s2"; "$s3"; "$s4"; "$s5"; "$s6"; "$s7" |]
let caller_fregs = Array.init 24 (fun i -> Printf.sprintf "$f%d" i)
let callee_fregs = Array.init 8 (fun i -> Printf.sprintf "$f%d" (i+24))
let allregs = (Array.to_list caller_regs) @ (Array.to_list callee_regs)
let allfregs = (Array.to_list caller_fregs) @ (Array.to_list callee_fregs)
let reg_zero = "$zero" (* zero register *)
let reg_cond = "$at"
let reg_gp = "$gp" (* global pointer *)
let reg_sp = "$sp" (* stack pointer *)
let reg_fp = "$fp"
let reg_ra = "$ra" (* return address *)
let is_reg x = (x.[0] = '$')
let not_reg x = not (is_reg x)

let is_mov t = function
    | Let(_, Mov(_)) -> if t = Type.Int then true else false
    | Let(_, FMov(_)) -> if t = Type.Float then true else false
    | _ -> false

(* variables sets of (int, float) *)
let id_or_imm_var = function
    | V(x) -> S.singleton x
    | _ -> S.empty
let exp_vars' = function
    | Nop | Li(_) | La(_) | FLw(_, L(_)) ->
            (S.empty, S.empty)
    | Mul(x) | Div(x) | Mov(x) | SLL(x, _) | SRL(x, _) | Lw(_, x) | Itof(x) | FLw(_, V(x)) ->
            (S.singleton x, S.empty)
    | FMov(x) | FNeg(x) | Ftoi(x) | FAbs(x) | FSqrt(x) ->
            (S.empty, S.singleton x)
    | Add(x, y') ->
            (S.add x (id_or_imm_var y'), S.empty)
    | Sub(x, y) -> (S.add x (S.singleton y), S.empty)
    | Sw(x, _, y) ->
            (S.of_list [x; y], S.empty)
    | FAdd(x, y) | FSub(x, y) | FMul(x, y) | FDiv(x, y) ->
            (S.empty, S.of_list [x; y])
    | FSw(x, _, y) | CAdd(x, y) ->
            (S.singleton y, S.singleton x)
    | Call(_, ys, zs) ->
            (S.of_list ys, S.of_list zs)
let rec instr_vars' = function
    | Label(_) | Jump(_) -> (S.empty, S.empty)
    | Ans(exp) -> exp_vars' exp
    | Let((x, t), exp) ->
            let (vars, fvars) = exp_vars' exp in
            (match t with
            | Type.Unit -> (vars, fvars)
            | Type.Float -> (vars, S.add x fvars)
            | _ -> (S.add x vars, fvars))
    | IfEq(x, y, _, _) -> (S.of_list [x; y], S.empty)
    | IfLT(x, y', _, _) -> (S.add x (id_or_imm_var y'), S.empty)
    | IfFEq(x, y, _, _) | IfFLT(x, y, _, _) -> (S.empty, S.of_list [x; y])
    | For(_, x, c, step, _, _, _) ->
            let (vars, fvars) = instr_vars' step in
            (S.add x vars, fvars)
(* eliminate reserved registers such as "$gp" *)
let is_valid_var x = (not_reg x) || (List.mem x allregs) || (List.mem x allfregs)
let var_filter (ivars, fvars) =
    (S.filter (fun v -> is_valid_var v) ivars, S.filter (fun v -> is_valid_var v) fvars)
let exp_vars exp = var_filter (exp_vars' exp)
let instr_vars exp = var_filter (instr_vars' exp)

let rec bind xt = function (* bind the result of Ir.t to xt *)
    | [] -> []
    | Ans(exp) :: rem ->
            (match bind xt rem with
            | [] -> [Let(xt, exp)] (* bind a return value *)
            | rem' -> (* bind return values <- 'if' case *)
                    let cont_l = Id.L(Id.genid "cont") in
                    Let(xt, exp) :: (Jump(cont_l) :: rem' @ [Jump(cont_l); Label(cont_l)]))
    | e :: rem -> e :: bind xt rem

let print_blank chan n =
    for i = 0 to n do
        Printf.fprintf chan " "
    done
let print_indent chan depth =
    for i = 1 to depth
    do
        Printf.fprintf chan "  "
    done
let rec print_t chan depth = function
    | Ans(e) ->
            print_indent chan (depth+1);
            print_exp chan e (depth+1)
    | Let ((x, _), e) ->
            print_indent chan (depth+1);
            Printf.fprintf chan "let %s = " x;
            print_exp chan e (depth+1);
    | Label(Id.L(s)) ->
            print_indent chan depth;
            Printf.fprintf chan "%s:\n" s
    | Jump(Id.L(s)) ->
            print_indent chan (depth+1);
            Printf.fprintf chan "jump %s\n" s
    | IfEq(x, y, Id.L(then_s), Id.L(else_s)) ->
            print_indent chan (depth+1);
            Printf.fprintf chan "if %s = %s then %s else %s\n" x y then_s else_s
    | IfLT(x, y', Id.L(then_s), Id.L(else_s)) ->
            print_indent chan (depth+1);
            (match y' with
            | V(v) -> Printf.fprintf chan "if %s < %s then %s else %s\n" x v then_s else_s
            | C(c) -> Printf.fprintf chan "if %s < %d then %s else %s\n" x c then_s else_s)
    | IfFEq(x, y, Id.L(then_s), Id.L(else_s)) ->
            print_indent chan (depth+1);
            Printf.fprintf chan "if %s =. %s then %s else %s\n" x y then_s else_s
    | IfFLT(x, y, Id.L(then_s), Id.L(else_s)) ->
            print_indent chan (depth+1);
            Printf.fprintf chan "if %s <. %s then %s else %s\n" x y then_s else_s
    | For(_, x, c, instr, Id.L(exit), Id.L(loop), is_parallel) ->
            print_indent chan (depth+1);
            if is_parallel then
                Printf.fprintf chan "Pfor (; %s >= %d; \n" x c
            else
                Printf.fprintf chan "for (; %s >= %d; \n" x c;
            print_t chan (depth+2) instr;
            print_indent chan (depth+1);
            Printf.fprintf chan "    ) loop %s exit %s\n" loop exit
and print_exp chan e depth =
    match e with
    | Nop -> Printf.fprintf chan "nop\n"
    | Add(x, y') ->
            (match y' with
            | V(v) -> Printf.fprintf chan "add %s, %s\n" x v
            | C(c) -> Printf.fprintf chan "addi %s, %d\n" x c)
    | Sub(x, y) -> Printf.fprintf chan "sub %s, %s\n" x y
    | Mul(x) -> Printf.fprintf chan "mul %s\n" x
    | Div(x) -> Printf.fprintf chan "div %s\n" x
    | SLL(x, i) -> Printf.fprintf chan "sll %s, %d\n" x i
    | SRL(x, i) -> Printf.fprintf chan "srl %s, %d\n" x i
    | Lw(c, x) -> Printf.fprintf chan "lw %d(%s)\n" c x
    | Li(c) -> Printf.fprintf chan "li %d\n" c
    | Mov(x) -> Printf.fprintf chan "mov %s\n" x
    | La(Id.L(l)) -> Printf.fprintf chan "la %s\n" l
    | Sw(x, c, y) -> Printf.fprintf chan "sw %s %d(%s)\n" x c y
    | FLw(c, V(x)) -> Printf.fprintf chan "lw.s %d(%s)\n" c x
    | FLw(_, L(Id.L(s))) -> Printf.fprintf chan "lw.s %s\n" s
    | FSw(x, c, y) -> Printf.fprintf chan "sw.s %s %d(%s)\n" x c y
    | FAdd(x, y) -> Printf.fprintf chan "add.s %s %s\n" x y
    | FSub(x, y) -> Printf.fprintf chan "sub.s %s %s\n" x y
    | FMul(x, y) -> Printf.fprintf chan "mul.s %s %s\n" x y
    | FDiv(x, y) -> Printf.fprintf chan "div.s %s %s\n" x y
    | FMov(x) -> Printf.fprintf chan "mov.s %s\n" x
    | FNeg(x) -> Printf.fprintf chan "neg.s %s\n" x
    | Ftoi(x) -> Printf.fprintf chan "ftoi %s\n" x
    | Itof(x) -> Printf.fprintf chan "itof %s\n" x
    | FAbs(x) -> Printf.fprintf chan "abs.s %s\n" x
    | FSqrt(x) -> Printf.fprintf chan "sqrt.s %s\n" x
    (* y := !y +. x *)
    | CAdd(x, y) -> Printf.fprintf chan "cadd %s %s\n" x y
    | Call(Id.L(x), ys, zs) ->
            Printf.fprintf chan "Call %s " x;
            List.iter (fun y -> Printf.fprintf chan "%s " y) ys;
            List.iter (fun z -> Printf.fprintf chan "%s " z) zs;
            Printf.fprintf chan "\n"
let print_fundef chan { name = Id.L(x); args = _; fargs = _; body = es; ret = _ } =
    Printf.fprintf chan "%s:\n" x;
    List.iter (print_t chan 1) es
let print chan (l, fundefs, es) =
    List.iter
        (fun fundef -> print_fundef chan fundef)
        fundefs;
    Printf.fprintf chan "MAIN\n";
    List.iter (print_t chan 1) es;
    (l, fundefs, es)
