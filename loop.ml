open Ir

let load ys ys' zs zs' =
    List.fold_right2
        (fun y y' e -> Let((y, Type.Int), Mov(y')) :: e)
        ys
        ys'
        (List.fold_right2
            (fun z z' e ->
                Let((z, Type.Float), FMov(z')) :: e)
            zs
            zs'
            [])

let rec isLoop s = function
    | [] -> assert false
    | Ans(exp) :: _ ->
            (match exp with
            | Call(Id.L(s'), _, _) when s = s' -> true
            | _ -> false)
    | _ :: rem -> isLoop s rem
let rec h' s ys zs = function
    | [] -> assert false
    | Ans(Call(Id.L(s'), ys', zs')) :: rem when s = s' -> 
            (load ys ys' zs zs') @ (Jump(Id.L(s ^ "_start")) :: h s ys zs rem)
    | _ :: rem -> h' s ys zs rem
and h s ys zs = function
    | [] -> []
    (* end part *)
    | Jump(Id.L(s')) as j :: rem when String.sub s' (String.length s' - 4) 4 = "_end" ->
            if isLoop s rem then h' s ys zs rem else j :: h s ys zs rem
    | e :: rem -> e :: h s ys zs rem

let g { name = Id.L(s); args = ys; fargs = zs; body = es; ret = t } =
    { name = Id.L(s); args = ys; fargs = zs; body = h s ys zs es; ret = t }

let f (data, fundefs, e) =
    (data, List.map g fundefs, e)
