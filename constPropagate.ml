open Ir

let rec nthExp' b = function
    | 0 -> List.hd b
    | j -> nthExp' (List.tl b) (j-1)
let rec nthExp bs = function
    | 0, j -> nthExp' (List.hd bs) j
    | i, j -> nthExp (List.tl bs) ((i-1), j)

let reach' bs in_out (i, j) x =
    let ret = ref [] in
    for k = 0 to j-1 do
        match nthExp bs (i, k) with
        | Let((y, _), _) when x = y -> ret := [(i, k)]
        | _ -> ()
    done;
    if !ret = [] then
        ReachDef.ExpS.iter
            (fun (i, j) ->
                match nthExp bs (i, j) with
                | Let((y, _), _) when x = y -> ret := (i, j) :: !ret
                | _ -> ())
            (fst in_out).(i);
    !ret
let reach bs in_out (i, j) x =
    let gens_kills = LiveAnaly.calcIrGenkill [nthExp bs (i, j)] in
    let ivars, fvars = fst (fst gens_kills), fst (snd gens_kills) in
    let vars = S.union ivars fvars in
    if S.mem x vars then
        reach' bs in_out (i, j) x
    else
        []

let use bs in_out (i, j) =
    let x =
        match nthExp bs (i, j) with
        | Let((y, _), _) -> y
        | _ -> assert false in
    let ret = ref [] in
    for i' = 0 to (List.length bs) - 1 do
        for j' = 0 to (List.length (List.nth bs i')) - 1 do
            if List.mem (i, j) (reach bs in_out (i', j') x) then
                ret := (i', j') :: !ret
        done;
    done;
    !ret

let rec constDefList' (i, j) = function
    | [] -> []
    | Let((_, Type.Int), Li(_)) :: rem -> (i, j) :: (constDefList' (i, (j+1)) rem)
    | _ :: rem -> constDefList' (i, (j+1)) rem
let rec constDefList i = function
    | [] -> []
    | b :: rem -> (constDefList' (i, 0) b) @ (constDefList (i+1) rem)

(* whether (i, j) of bs is definition expression of immidiate *)
let isDefOfImm bs (i, j) imm =
    match nthExp bs (i, j), imm with
    | Let((_, Type.Int), Li(i)), j when i = j -> true
    | _ -> false

let replaceImm_exp exp x imm =
    match exp with
    | Add(y, V(z)) ->
            (* Printf.printf "replace %s %d\n" x imm; *)
            if x = y && x = z then Li(imm*2)
            else if x = y then Add(z, C(imm))
            else if x = z then Add(y, C(imm))
            else Add(y, V(z))
    | Add(y, C(c)) ->
            if x = y then Li(imm + c)
            else Add(y, C(c))
    | Sub(y, z) ->
            if x = z then Add(y, C(-imm))
            else Sub(y, z)
    | Mul(y) when x = y -> Li(imm * 4)
    | Div(y) when x = y -> Li(imm / 2)
    | SLL(y, i) when x = y -> Li(Int.shift_left imm i)
    | SRL(y, i) when x = y -> Li(Int.shift_right imm i)
    | Mov(y) when x = y -> Li(imm)
    | exp -> exp
let replaceImm'' x imm = function
    | Ans(exp) -> Ans(replaceImm_exp exp x imm)
    | Let((y, t), exp) -> Let((y, t), replaceImm_exp exp x imm)
    | IfLT(y, V(z), l1, l2) ->
            if x = z then IfLT(y, C(imm), l1, l2)
            else IfLT(y, V(z), l1, l2)
    | e -> e
let rec replaceImm' j x imm = function
    | [] -> []
    | e :: rem ->
            if j = 0 then
                (replaceImm'' x imm e) :: rem
            else
                e :: replaceImm' (j-1) x imm rem
let rec replaceImm (i, j) x imm = function
    | [] -> []
    | b :: rem ->
            if i = 0 then
                (replaceImm' j x imm b) :: rem
            else
                b :: replaceImm (i-1, j) x imm rem

let h bs in_out =
    let bs_p = ref bs in
    let work_list = ref (constDefList 0 !bs_p) in
    while !work_list != [] do
        let (i, j) = List.hd !work_list in
        work_list := List.tl !work_list;
        let x, imm =
            match nthExp !bs_p (i, j) with
            | Let((y, Type.Int), Li(imm)) -> y, imm
            | _ -> assert false in
        (* debug *)
            (* Printf.printf "x:%s, imm:%d\n" x imm; *)
            (* Printf.printf "reach\n"; *)
            (* List.iter (fun (i, j) -> Printf.printf "(%d, %d) " i j;) (reach !bs_p in_out (i, j) x); *)
            (* Printf.printf "\nuse\n"; *)
            (* List.iter (fun (i, j) -> Printf.printf "(%d, %d) " i j;) (use !bs_p in_out (i, j)); *)
            (* Printf.printf "\n"; *)
        List.iter
            (fun (i, j) ->
                bs_p :=
                    (* check if all definitions are the same imm *)
                    if List.for_all (fun exp -> isDefOfImm !bs_p exp imm) (reach !bs_p in_out (i, j) x) then
                        replaceImm (i, j) x imm !bs_p
                    else
                        !bs_p;
                match nthExp !bs_p (i, j) with
                | Let(_, Li(_)) -> work_list := (i, j) :: !work_list
                | _ -> ())
            (use !bs_p in_out (i, j))
    done;
    !bs_p

let g { name = Id.L(s); args = ys; fargs = zs; Block.body_block = bs; ret = t } =
    (* Printf.printf "func: %s\n" s; *)
    let in_out = ReachDef.f bs in
    (* ReachDef.print in_out; *)
    let bs = h bs in_out in
    { name = Id.L(s); args = ys; fargs = zs; Block.body_block = bs; ret = t }

let f (data, fundefs, e) =
    let in_out = ReachDef.f e in
    (data, List.map (fun fundef -> g fundef) fundefs, h e in_out)
