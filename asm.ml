open Ir

type t =
    (* (mnemonic, dst, src, imm, label) *)
    | Instr of string * Id.t option * Id.t list * int option * Id.l list
    | Label of Id.l
type fundef = { name : Id.l; args : Id.t list; fargs : Id.t list; body_block : t list list; ret : Type.t }

let isMove t instr =
    match t, instr with
    | Type.Int, Instr("move", _, _, _, _) -> true
    | Type.Float, Instr("mov.s", _, _, _, _) -> true
    | _ -> false

let instr_vars = function
    | Instr("nop", Some(x), [], None, []) -> (S.empty, S.empty)
    | Instr("add", Some(x), [y; v], None, []) -> (S.of_list [x; y; v], S.empty)
    | Instr("addi", Some(x), [y], Some(c), []) -> (S.of_list [x; y], S.empty)
    | Instr("sub", Some(x), [y; z], None, []) -> (S.of_list [x; y; z], S.empty)
    | Instr("sll", Some(x), [y], Some(c), []) -> (S.of_list [x; y], S.empty)
    | Instr("srl", Some(x), [y], Some(c), []) -> (S.of_list [x; y], S.empty)
    | Instr("lw", Some(x), [y], Some(c), []) -> (S.of_list [x; y], S.empty)
    | Instr("li", Some(x), [], Some(c), []) -> (S.singleton x, S.empty)
    | Instr("move", Some(x), [y], None, []) -> (S.of_list [x; y], S.empty)
    | Instr("la", Some(x), [], None, [l]) -> (S.singleton x, S.empty)
    | Instr("sw", None, [x; y], Some(c), []) -> (S.of_list [x; y], S.empty)
    | Instr("lw.s", Some(x), [y], Some(c), []) -> (S.singleton y, S.singleton x)
    | Instr("lw.s", Some(x), [], None, [Id.L(s)]) -> (S.empty, S.singleton x)
    | Instr("sw.s", None, [x; y], Some(c), []) -> (S.singleton y, S.singleton x)
    | Instr("add.s", Some(x), [y; z], None, []) -> (S.empty, S.of_list [x; y; z])
    | Instr("sub.s", Some(x), [y; z], None, []) -> (S.empty, S.of_list [x; y; z])
    | Instr("mul.s", Some(x), [y; z], None, []) -> (S.empty, S.of_list [x; y; z])
    | Instr("div.s", Some(x), [y; z], None, []) -> (S.empty, S.of_list [x; y; z])
    | Instr("mov.s", Some(x), [y], None, []) -> (S.empty, S.of_list [x; y])
    | Instr("neg.s", Some(x), [y], None, []) -> (S.empty, S.of_list [x; y])
    | Instr("ftoi", Some(x), [y], None, []) -> (S.singleton x, S.singleton y)
    | Instr("itof", Some(x), [y], None, []) -> (S.singleton y, S.singleton x)
    | Instr("abs.s", Some(x), [y], None, []) -> (S.empty, S.of_list [x; y])
    | Instr("sqrt.s", Some(x), [y], None, []) -> (S.empty, S.of_list [x; y])
    | Instr("cadd", None, [x; y], None, []) -> (S.singleton y, S.singleton x)
    | Instr("jal", _, [_; _], None, [l]) -> (S.empty, S.empty)
    | Instr("jr", None, [x; y], None, []) -> (S.of_list [x; y], S.empty)
    | Instr("j", None, _, None, [l]) -> (S.empty, S.empty)
    | Instr("beq", None, [x; y], None, [l0; l1]) -> (S.of_list [x; y], S.empty)
    | Instr("bne", None, [x; y], None, [l0; l1]) -> (S.of_list [x; y], S.empty)
    | Instr("slt", Some(x), [y; z], None, []) -> (S.of_list [x; y; z], S.empty)
    | Instr("slti", Some(x), [y], Some(c), []) -> (S.of_list [x; y], S.empty)
    | Instr("c.eq.s", None, [x; y], Some(0), []) -> (S.empty, S.of_list [x; y])
    | Instr("c.lt.s", None, [x; y], Some(0), []) -> (S.empty, S.of_list [x; y])
    | Instr("bf.s", None, [], Some(0), [l0; l1]) -> (S.empty, S.empty)
    | Instr("fork", None, [x], Some(c), []) -> (S.singleton x, S.empty)
    | Instr("next", Some(x), [y], None, []) when x = y -> (S.singleton x, S.empty)
    | Instr("end", None, [], None, []) -> (S.empty, S.empty)
    | Label(_) -> (S.empty, S.empty)
    | _ -> assert false

(*
 Block of Asm.t
 *)
let rec toBlock = function
    | [] -> []
    | Label(l) :: rem as is -> toBlock' is
    | _ -> assert false
and toBlock' = function
    | [] -> []
    | Instr("jr", _, _, _, _) as i :: rem -> [i] :: toBlock rem
    | Instr("j", _, _, _, _) as i :: rem -> [i] :: toBlock rem
    | Instr("beq", _, _, _, _) as i :: rem -> [i] :: toBlock rem
    | Instr("bne", _, _, _, _) as i :: rem -> [i] :: toBlock rem
    | Instr("bf.s", _, _, _, _) as i :: rem -> [i] :: toBlock rem
    | i :: rem ->
            match toBlock' rem with
            | [] -> assert false
            | i' :: rem' ->
                    (i :: i') :: rem'
let getLabel = function
    | Label(l) :: _ -> l
    | _ -> assert false
let rec succLabels = function
    | [] -> assert false
    | [Instr(mne, _, _, _, labels)] ->
            (* (1* debug *1) Printf.printf "%s\n" mne; *)
            (* assert (labels != []); *)
            labels
    | _ :: rem -> succLabels rem

(*
 Convert Ir to Asm
 *)
(* insert step and rename the return label for "FOR" *)
let rec insertStep base step = function
    | [] -> Printf.printf "%s\n" base; assert false
    | Jump(Id.L(s)) :: rem when base = s -> step :: Jump(Id.L(s)) :: rem
    | e :: rem -> e :: insertStep base step rem
(* insert step and rename the return label for parallel "FOR" *)
let rec insertPStep base base' = function
    | [] -> assert false
    | Jump(Id.L(s)) :: rem when base = s -> Jump(Id.L(base')) :: rem
    | e :: rem -> e :: insertPStep base base' rem
(* set arg-regs to arg-vars, and save callee regs *)
let prologue int float =
    List.fold_right2
        (fun i x e -> Instr("move", Some(x), [Ir.caller_regs.(i)], None, []) :: e)
        (List.init (List.length int) (fun x -> x))
        int
        (List.fold_right2
            (fun j y e ->
                Instr("mov.s", Some(y), [Ir.caller_fregs.(j)], None, []) :: e)
            (List.init (List.length float) (fun x -> x))
            float
            [])
(* set arg-vars to arg-regs *)
let load int float =
    List.fold_right2
        (fun i x e -> Instr("move", Some(Ir.caller_regs.(i)), [x], None, []) :: e)
        (List.init (List.length int) (fun x -> x))
        int
        (List.fold_right2
            (fun j y e ->
                Instr("mov.s", Some(Ir.caller_fregs.(j)), [y], None, []) :: e)
            (List.init (List.length float) (fun x -> x))
            float
            [])
let epilogue saved_vars saved_fvars =
    List.fold_left2
        (fun e r x -> Instr("move", Some(r), [x], None, []) :: e)
        (List.fold_left2
            (fun e r x -> Instr("mov.s", Some(r), [x], None, []) :: e)
            []
            (Array.to_list Ir.callee_fregs)
            saved_fvars)
        (Array.to_list Ir.callee_regs)
        saved_vars
let to_regs ys =
    List.init (List.length ys) (fun i -> Ir.caller_regs.(i))
let to_fregs zs =
    List.init (List.length zs) (fun i -> Ir.caller_fregs.(i))
let h_exp x = function
    | Nop -> []
    | Add(y, z') ->
            (match z' with
            | V(v) -> [Instr("add", Some(x), [y; v], None, [])]
            | C(c) -> [Instr("addi", Some(x), [y], Some(c), [])])
    | Sub(y, z) -> [Instr("sub", Some(x), [y; z], None, [])]
    | Mul(y) -> [Instr("sll", Some(x), [y], Some(2), []) (* "<< 2" *)]
    | Div(y) -> [Instr("srl", Some(x), [y], Some(1), []) (* "> 1" *)]
    | SLL(y, c) -> [Instr("sll", Some(x), [y], Some(c), [])]
    | SRL(y, c) -> [Instr("srl", Some(x), [y], Some(c), [])]
    | Lw(c, y) -> [Instr("lw", Some(x), [y], Some(c), [])]
    | Li(c) -> [Instr("li", Some(x), [], Some(c), [])]
    | Mov(y) -> [Instr("move", Some(x), [y], None, [])]
    | La(l) -> [Instr("la", Some(x), [], None, [l])]
    | Sw(x, c, y) -> [Instr("sw", None, [x; y], Some(c), [])]
    | FLw(c, V(y)) -> [Instr("lw.s", Some(x), [y], Some(c), [])]
    | FLw(_, L(l)) -> [Instr("lw.s", Some(x), [], None, [l])]
    | FSw(x, c, y) -> [Instr("sw.s", None, [x; y], Some(c), [])]
    | FAdd(y, z) -> [Instr("add.s", Some(x), [y; z], None, [])]
    | FSub(y, z) -> [Instr("sub.s", Some(x), [y; z], None, [])]
    | FMul(y, z) -> [Instr("mul.s", Some(x), [y; z], None, [])]
    | FDiv(y, z) -> [Instr("div.s", Some(x), [y; z], None, [])]
    | FMov(y) -> [Instr("mov.s", Some(x), [y], None, [])]
    | FNeg(y) -> [Instr("neg.s", Some(x), [y], None, [])]
    | Ftoi(y) -> [Instr("ftoi", Some(x), [y], None, [])]
    | Itof(y) -> [Instr("itof", Some(x), [y], None, [])]
    | FAbs(y) -> [Instr("abs.s", Some(x), [y], None, [])]
    | FSqrt(y) -> [Instr("sqrt.s", Some(x), [y], None, [])]
    | CAdd(x, y) -> [Instr("cadd", None, [x; y], None, [])]
    | Call(l, ys, zs) ->
            let ss = 4 in
            let args = [string_of_int (List.length ys); string_of_int (List.length zs)] in
            Instr("sw", None, [reg_ra; reg_sp], Some(ss-4), []) ::
                Instr("addi", Some(reg_sp), [reg_sp], Some(ss), []) ::
                    Instr("jal", Some(x), args, None, [l]) ::
                        Instr("addi", Some(reg_sp), [reg_sp], Some(-ss), []) ::
                            [Instr("lw", Some(reg_ra), [reg_sp], Some(ss-4), [])]
let rec h' t = function
    | [] -> []
    | Ans(Call(l, ys, zs)) :: rem ->
            if (List.length ys) > (Array.length caller_regs) then
                failwith "int arguments are too many";
            if (List.length zs) > (Array.length caller_fregs) then
                failwith "float arguments are too many";
            let args = [string_of_int (List.length ys); string_of_int (List.length zs)] in
            (load ys zs) @
                [Instr("j", None, args, None, [l])] @
                    h' t rem
    | Ans(exp) :: rem ->
            (match t with
            | Type.Float ->
                    (h_exp "$f0" exp) @
                        [Instr("jr", None, ["$f0"; Ir.reg_ra], None, [])] @
                            h' t rem
            | _ ->
                    (h_exp "$a0" exp) @
                        [Instr("jr", None, ["$a0"; Ir.reg_ra], None, [])] @
                            h' t rem)
    | Let((x, t'), Call(l, ys, zs)) :: rem ->
            if (List.length ys) > (Array.length caller_regs) then
                failwith "int arguments are too many";
            if (List.length zs) > (Array.length caller_fregs) then
                failwith "float arguments are too many";
            (match t' with
            | Type.Unit ->
                    (load ys zs) @
                    (h_exp x (Call(l, ys, zs))) @
                    h' t rem
            | Type.Float ->
                    (load ys zs) @
                    (h_exp "$f0" (Call(l, ys, zs))) @
                    (h_exp x (FMov("$f0"))) @
                    h' t rem
            | _ ->
                    (load ys zs) @
                    (h_exp "$a0" (Call(l, ys, zs))) @
                    (h_exp x (Mov("$a0"))) @
                    h' t rem)
    | Let((x, _), exp) :: rem ->
            let instr = h_exp x exp in
            instr @ h' t rem
    | Label(l) :: rem ->
            Label(l) :: h' t rem
    | Jump(l) :: rem ->
            Instr("j", None, ["0"; "0"], None, [l]) :: h' t rem
    | IfEq(x, y, l0, l1) :: rem ->
            Instr("bne", None, [x; y], None, [l0; l1]) :: h' t rem
    | IfLT(x, V(y), l0, l1) :: rem ->
            Instr("slt", Some(reg_cond), [x; y], None, []) ::
                Instr("beq", None, [reg_zero; reg_cond], None, [l0; l1]) ::
                    h' t rem
    | IfLT(x, C(c), l0, l1) :: rem ->
            Instr("slti", Some(reg_cond), [x], Some(c), []) ::
                Instr("beq", None, [reg_zero; reg_cond], None, [l0; l1]) ::
                    h' t rem
    | IfFEq(x, y, l0, l1) :: rem ->
            Instr("c.eq.s", None, [x; y], Some(0), []) ::
                Instr("bf.s", None, [], Some(0), [l0; l1]) ::
                    h' t rem
    | IfFLT(x, y, l0, l1) :: rem ->
            Instr("c.lt.s", None, [x; y], Some(0), []) ::
                Instr("bf.s", None, [], Some(0), [l0; l1]) ::
                    h' t rem
    | For(base, x, c, step, l0, l1, is_parallel) :: rem ->
            let Id.L(s) = base in
            if is_parallel then
                (match step with
                | Let((reg, Type.Int), Add(reg', C(c'))) when reg = reg' ->
                        let s' = Id.genid (s ^ "_parallel") in
                        let parallel_l = Id.L(s') in
                        Instr("fork", None, [reg], Some(c'), []) ::
                            Instr("j", None, ["0"; "0"], None, [parallel_l]) ::
                                Label(parallel_l) ::
                                    Instr("next", Some(reg), [reg], None, []) ::
                                        Instr("slti", Some(reg_cond), [x], Some(c), []) ::
                                            Instr("beq", None, [reg_zero; reg_cond], None, [l0; l1]) ::
                        (match rem with
                        | Ir.Label(l) :: rem ->
                                Label(l) ::
                                    Instr("end", None, [], None, []) ::
                                        h' t (insertPStep s s' rem)
                        | _ -> assert false)
                | _ -> assert false)
            else
                Instr("slti", Some(reg_cond), [x], Some(c), []) ::
                    Instr("beq", None, [reg_zero; reg_cond], None, [l0; l1]) ::
                        h' t (insertStep s step rem)
(* insert arguments loading before jump to "_start" *)
let rec h s ys zs t = function
    | Ir.Label(Id.L(s')) :: rem when s = s' ->
            Label(Id.L(s)) :: h s ys zs t rem
    | Ir.Jump(Id.L(s')) as j :: rem when s ^ "_start" = s' ->
            prologue ys zs @ h' t (j :: rem)
    | Let((x, Type.Int), Mov(y)) :: rem when is_reg y ->
            Instr("move", Some(x), [y], None, []) :: h s ys zs t rem
    | Let((x, Type.Float), FMov(y)) :: rem when is_reg y ->
            Instr("mov.s", Some(x), [y], None, []) :: h s ys zs t rem
    | _ -> assert false

let g { name = Id.L(s); args = ys; fargs = zs; Block.body_block = bs; ret = t} =
    { name = Id.L(s); args = ys; fargs = zs; body_block = toBlock (h s ys zs t (List.flatten bs)); ret = t }

let f (data, fundefs, bs) =
    let fundefs = List.map (fun fundef -> g fundef) fundefs in
    let block = toBlock (h' Type.Unit (List.flatten bs)) in
    (data, fundefs, block)

(*
 Print functions
 *)
let printDst chan = function
    | None -> Printf.fprintf chan "dst:None "
    | Some(x) -> Printf.fprintf chan "dst:%s " x
let printSrc chan = function
    | [] -> Printf.fprintf chan "src:None "
    | [x] -> Printf.fprintf chan "src:%s " x
    | [x; y] -> Printf.fprintf chan "src:%s %s " x y
    | _ -> assert false
let printImm chan = function
    | None -> Printf.fprintf chan "imm:None "
    | Some(c) -> Printf.fprintf chan "imm:%d " c
let printLabel chan = function
    | [] -> Printf.fprintf chan "label:None "
    | [Id.L(s)] -> Printf.fprintf chan "label:%s " s
    | [Id.L(s0); Id.L(s1)] -> Printf.fprintf chan "label:%s %s " s0 s1
    | _ -> assert false
let printAsm chan depth = function
    | Instr(mne, dst, src, imm, label) ->
            print_indent chan (depth+1);
            Printf.fprintf chan "%s " mne;
            printDst chan dst;
            printSrc chan src;
            printImm chan imm;
            printLabel chan label;
            Printf.fprintf chan "\n"
    | Label(Id.L(s)) ->
            print_indent chan depth;
            Printf.fprintf chan "%s:\n" s
let printBlock chan depth b =
    List.iter
        (fun i -> printAsm chan depth i)
        b;
    Printf.fprintf chan "\n"
let printFundef chan { name = Id.L(x); args = _; fargs = _; body_block = bs; ret = _ } =
    Printf.fprintf chan "%s:\n" x;
    List.iter
        (fun b -> printBlock chan 1 b)
        bs
let print chan (data, fundefs, bs) =
    List.iter
        (fun fundef -> printFundef chan fundef)
        fundefs;
    Printf.fprintf chan "Main\n";
    List.iter
        (fun b -> printBlock chan 1 b)
        bs;
    (data, fundefs, bs)
