{
    open Yacc
    open Type
}

let space = [' ' '\t' '\r']
let digit = ['0'-'9']
let lower = ['a'-'z']
let upper = ['A'-'Z']

rule token = parse
    | space+
        { token lexbuf }
    | "\n"
        { Lexing.new_line lexbuf; token lexbuf }
    | "(*"
        { comment lexbuf; (* for nested comments *)
          token lexbuf}
    | '('
        { LPAREN }
    | ')'
        { RPAREN }
    | "true"
        { BOOL(true) }
    | "false"
        { BOOL(false) }
    | "not"
        { NOT }
    | digit+
        { INT(int_of_string (Lexing.lexeme lexbuf)) }
    | digit+ ('.' digit*)? (['e' 'E'] ['+' '-']? digit+)?
        { FLOAT(float_of_string (Lexing.lexeme lexbuf)) }
    | '-'
        { MINUS }
    | '+'
        { PLUS }
    | '*'
        { AST }
    | '/'
        { SLASH }
    | "-."
        { MINUS_DOT }
    | "+."
        { PLUS_DOT }
    | "*."
        { AST_DOT }
    | "/."
        { SLASH_DOT }
    | '='
        { EQUAL }
    | "<>"
        { LESS_GREATER }
    | "<="
        { LESS_EQUAL }
    | ">="
        { GREATER_EQUAL }
    | '<'
        { LESS }
    | '>'
        { GREATER }
    | "if"
        { IF }
    | "then"
        { THEN }
    | "else"
        { ELSE }
    | "let"
        { LET }
    | "in"
        { IN }
    | "rec"
        { REC }
    | ','
        { COMMA }
    | '_'
        { WILDCARD }
    | "create_array" | "Array.create" | "Array.make"
        { ARRAY_CREATE }
    | '.'
        { DOT }
    | "<-"
        { LESS_MINUS }
    | ';'
        { SEMICOLON }
    | eof
        { EOF }
    | lower (digit|lower|upper|'_')*
        { IDENT(Lexing.lexeme lexbuf) }
    | _
        { failwith
            (Printf.sprintf "unknown token %s near characters %d-%d"
                (Lexing.lexeme lexbuf)
                (Lexing.lexeme_start lexbuf)
                (Lexing.lexeme_end lexbuf)) }
and comment = parse
    | "*)"
        { () }
    | "(*"
        { comment lexbuf;
          comment lexbuf }
    | eof
        { Format.eprintf "warning: unterminated comment@." }
    | "\n"
        { Lexing.new_line lexbuf; comment lexbuf }
    | _
        { comment lexbuf }
