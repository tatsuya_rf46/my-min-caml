open Asm

let hint = [(Id.L("iter.13"), 12); (Id.L("iter_trace_diffuse_rays.3084"), 8)]

let alloc i x =
    if i = x then reg_fp else x
let alloc' i = function
    | C(c) -> C(c)
    | V(x) -> V(alloc i x)
let alloc'' i = function
    | L(l) -> L(l)
    | V(x) -> V(alloc i x)

let rec h param = function
    | For((i, n, m), e1, e2) -> Let((reg_fp, Type.Int), Mov(i), For((i, n, m), delete_step i param e1, e2))
    | _ -> assert false
(* allocate 'index' to reg_fp and delete step sentences *)
and delete_step i param = function
    | Ans(exp) -> Ans(delete_step' i exp)
    | Let(xt, exp, e) ->
            if param = 0 then delete_step i param e else Let(xt, delete_step' i exp, delete_step i (param - 1) e)
    | Renew(xt, exp, e) ->
            if param = 0 then delete_step i param e else Renew(xt, delete_step' i exp, delete_step i (param - 1) e)
    | _ -> assert false
and delete_step' i = function
    | Add(x, y') -> Add(alloc i x, alloc' i y')
    | Sub(x, y') -> Sub(alloc i x, alloc' i y')
    | Mul(x) -> Mul(alloc i x)
    | Div(x) -> Div(alloc i x)
    | SLL(x, c) -> SLL(alloc i x, c)
    | SRL(x, c) -> SRL(alloc i x, c)
    | Lw(c, x) -> Lw(c, alloc i x)
    | Mov(x) -> Mov(alloc i x)
    | Sw(x, c, y) -> Sw(alloc i x, c, alloc i y)
    | FLw(c, x') -> FLw(c, alloc'' i x')
    | FSw(x, c, y) -> FSw(alloc i x, c, alloc i y)
    | FAdd(x, y) -> FAdd(alloc i x, alloc i y)
    | FSub(x, y) -> FSub(alloc i x, alloc i y)
    | FMul(x, y) -> FMul(alloc i x, alloc i y)
    | FDiv(x, y) -> FDiv(alloc i x, alloc i y)
    | FMov(x) -> FMov(alloc i x)
    | FNeg(x) -> FNeg(alloc i x)
    | Ftoi(x) -> Ftoi(alloc i x)
    | Itof(x) -> Itof(alloc i x)
    | FAbs(x) -> FAbs(alloc i x)
    | FSqrt(x) -> FSqrt(alloc i x)
    | CAdd(x, y) -> CAdd(alloc i x, alloc i y)
    | IfEq(x, y', e1, e2) -> IfEq(alloc i x, alloc' i y', delete_step i (-1) e1, delete_step i (-1) e2)
    | IfLT(x, y', e1, e2) -> IfLT(alloc i x, alloc' i y', delete_step i (-1) e1, delete_step i (-1) e2)
    | IfFEq(x, y, e1, e2) -> IfFEq(alloc i x, alloc i y, delete_step i (-1) e1, delete_step i (-1) e2)
    | IfFLT(x, y, e1, e2) -> IfFLT(alloc i x, alloc i y, delete_step i (-1) e1, delete_step i (-1) e2)
    | Loop(_, ys, zs) -> ForLoop(ys, zs)
    | _ as exp -> exp

let g { name = x; args = ys; fargs = zs; body = e; ret = t } =
    match List.assoc_opt x hint with
    | Some(param) ->
            { name = x; args = ys; fargs = zs; body = h param e; ret = t }
    | None ->
            { name = x; args = ys; fargs = zs; body = e; ret = t }

let f (data, fundefs, e) =
    (data, List.map g fundefs, e)
