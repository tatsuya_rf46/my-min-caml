open Ir
open Asm
open CtlFlowGraph

(* calculate Ir.t generations and kills of a block *)
let rec calcIrGenkill' = function
    | [] -> ((S.empty, S.empty), (S.empty, S.empty))
    | Ans(exp) :: _ -> ((fst (exp_vars exp), S.empty), ((snd (exp_vars exp)), S.empty))
    | Jump(_) :: _ -> ((S.empty, S.empty), (S.empty, S.empty))
    | IfEq(x, y, _, _) :: _ ->
            ((S.of_list [x; y], S.empty), (S.empty, S.empty))
    | IfLT(x, y', _, _) :: _ ->
            ((S.add x (id_or_imm_var y'), S.empty), (S.empty, S.empty))
    | IfFEq(x, y, _, _) :: _ | IfFLT(x, y, _, _) :: _ ->
            ((S.empty, S.empty), (S.of_list [x; y], S.empty))
    | Label(_) :: rem -> calcIrGenkill' rem
    | Let((x, t), exp) :: rem ->
            let (vars, fvars) = exp_vars exp in
            let ((gen, kill), (fgen, fkill)) = calcIrGenkill' rem in
            let (kill, fkill) =
                (match exp with
                | Call(_) -> (* define all caller-regs and caller-fregs *)
                        (S.union (S.of_list (Array.to_list caller_regs)) kill,
                            S.union (S.of_list (Array.to_list caller_fregs)) fkill)
                | _ -> (kill, fkill)) in
            (match t with
            | Type.Unit ->
                    ((S.union vars gen, kill), (S.union fvars fgen, fkill))
            | Type.Float ->
                    let gen' = S.union vars gen in
                    let fgen' = S.union fvars (S.diff fgen (S.singleton x)) in
                    let fkill' = S.add x fkill in
                    ((gen', kill), (fgen', fkill'))
            | _ ->
                    let gen' = S.union vars (S.diff gen (S.singleton x)) in
                    let kill' = S.add x kill in
                    let fgen' = S.union fvars fgen in
                    ((gen', kill'), (fgen', fkill)))
    | For(_, x, c, instr, l0, l1, _) :: _ ->
            calcIrGenkill' (instr :: [IfLT(x, C(c), l0, l1)])
(* eliminate reserved registers such as "$gp" *)
let calcIrGenkill b =
    let (igk, fgk) = calcIrGenkill' b in
    (Ir.var_filter igk, Ir.var_filter fgk)

(* calculate Asm.t generations and kills of a block *)
let rec calcAsmGenkill' = function
    | [] -> ((S.empty, S.empty), (S.empty, S.empty))
    | Instr("add", Some(x), src, None, []) :: rem
    | Instr("sub", Some(x), src, None, []) :: rem
    | Instr("slt", Some(x), src, None, []) :: rem
    | Instr("addi", Some(x), src, Some(_), []) :: rem
    | Instr("sll", Some(x), src, Some(_), []) :: rem
    | Instr("srl", Some(x), src, Some(_), []) :: rem
    | Instr("lw", Some(x), src, Some(_), []) :: rem
    | Instr("move", Some(x), src, None, []) :: rem
    | Instr("slti", Some(x), src, Some(_), []) :: rem
    | Instr("li", Some(x), src, Some(_), []) :: rem
    | Instr("la", Some(x), src, None, [_]) :: rem
    | Instr("next", Some(x), src, None, []) :: rem ->
            let ((gen, kill), (fgen, fkill)) = calcAsmGenkill' rem in
            let gen' = S.union (S.of_list src) (S.remove x gen) in
            let kill' = S.add x kill in
            ((gen', kill'), (fgen, fkill))
    | Instr("sw", None, src, Some(_), []) :: rem
    | Instr("fork", None, src, Some(_), []) :: rem
    | Instr("end", None, src, None, []) :: rem ->
            let ((gen, kill), (fgen, fkill)) = calcAsmGenkill' rem in
            let gen' = S.union (S.of_list src) gen in
            ((gen', kill), (fgen, fkill))
    | Instr("lw.s", Some(x), [], None, [_]) :: rem ->
            let ((gen, kill), (fgen, fkill)) = calcAsmGenkill' rem in
            let fgen' = S.remove x fgen in
            let fkill' = S.add x fkill in
            ((gen, kill), (fgen', fkill'))
    | Instr("lw.s", Some(x), [y], Some(_), []) :: rem
    | Instr("itof", Some(x), [y], None, []) :: rem ->
            let ((gen, kill), (fgen, fkill)) = calcAsmGenkill' rem in
            let gen' = S.add y gen in
            let fgen' = S.remove x fgen in
            let fkill' = S.add x fkill in
            ((gen', kill), (fgen', fkill'))
    | Instr("sw.s", None, [x; y], Some(_), []) :: rem
    | Instr("cadd", None, [x; y], None, []) :: rem ->
            let ((gen, kill), (fgen, fkill)) = calcAsmGenkill' rem in
            let gen' = S.add y gen in
            let fgen' = S.add x fgen in
            ((gen', kill), (fgen', fkill))
    | Instr("add.s", Some(x), src, None, []) :: rem
    | Instr("sub.s", Some(x), src, None, []) :: rem
    | Instr("mul.s", Some(x), src, None, []) :: rem
    | Instr("div.s", Some(x), src, None, []) :: rem
    | Instr("mov.s", Some(x), src, None, []) :: rem
    | Instr("neg.s", Some(x), src, None, []) :: rem
    | Instr("abs.s", Some(x), src, None, []) :: rem
    | Instr("sqrt.s", Some(x), src, None, []) :: rem ->
            let ((gen, kill), (fgen, fkill)) = calcAsmGenkill' rem in
            let fgen' = S.union (S.of_list src) (S.remove x fgen) in
            let fkill' = S.add x fkill in
            ((gen, kill), (fgen', fkill'))
    | Instr("ftoi", Some(x), [y], None, []) :: rem ->
            let ((gen, kill), (fgen, fkill)) = calcAsmGenkill' rem in
            let gen' = S.remove x gen in
            let kill' = S.add x kill in
            let fgen' = S.add y fgen in
            ((gen', kill'), (fgen', fkill))
    | Instr("c.eq.s", None, src, Some(_), []) :: rem
    | Instr("c.lt.s", None, src, Some(_), []) :: rem ->
            let ((gen, kill), (fgen, fkill)) = calcAsmGenkill' rem in
            let fgen' = S.union (S.of_list src) fgen in
            ((gen, kill), (fgen', fkill))
    | Instr("jal", dst, [arg_num; farg_num], None, [_]) :: rem ->
            let (arg_num, farg_num) = (int_of_string arg_num, int_of_string farg_num) in
            let (args, fargs) =
                (S.of_list (List.init arg_num (fun i -> Ir.caller_regs.(i))),
                    S.of_list (List.init farg_num (fun i -> Ir.caller_fregs.(i)))) in
            let ((gen, kill), (fgen, fkill)) = calcAsmGenkill' rem in
            let (kill', fkill') =
                (S.union (S.of_list (Array.to_list caller_regs)) kill,
                    S.union (S.of_list (Array.to_list caller_fregs)) fkill) in
            (match dst with
            | Some("$a0") ->
                    let gen' = S.union args (S.remove "$a0" gen) in
                    let fgen' = S.union fargs fgen in
                    ((gen', kill'), (fgen', fkill'))
            | Some("$f0") ->
                    let gen' = S.union args gen in
                    let fgen' = S.union fargs (S.remove "$f0" fgen) in
                    ((gen', kill'), (fgen', fkill'))
            | _ ->
                    ((S.union args gen, kill'), (S.union fargs fgen, fkill')))
    | Instr("j", None, [arg_num; farg_num], None, [_]) :: rem ->
            let (arg_num, farg_num) = (int_of_string arg_num, int_of_string farg_num) in
            let (args, fargs) =
                (S.of_list (List.init arg_num (fun i -> Ir.caller_regs.(i))),
                    S.of_list (List.init farg_num (fun i -> Ir.caller_fregs.(i)))) in
            let ((gen, kill), (fgen, fkill)) = calcAsmGenkill' rem in
            ((S.union args gen, kill), (S.union fargs fgen, fkill))
    | Instr("jr", None, ["$a0"; reg_ra], None, []) :: rem ->
            let ((gen, kill), (fgen, fkill)) = calcAsmGenkill' rem in
            let gen' = S.union (S.of_list ["$a0"; reg_ra]) gen in
            ((gen', kill), (fgen, fkill))
    | Instr("jr", None, ["$f0"; reg_ra], None, []) :: rem ->
            let ((gen, kill), (fgen, fkill)) = calcAsmGenkill' rem in
            let gen' = S.add reg_ra gen in
            let fgen' = S.add "$f0" fgen in
            ((gen', kill), (fgen', fkill))
    | Instr("beq", None, src, None, [_; _]) :: rem
    | Instr("bne", None, src, None, [_; _]) :: rem
    | Instr("bf.s", None, src, Some(_), [_; _]) :: rem ->
            let ((gen, kill), (fgen, fkill)) = calcAsmGenkill' rem in
            let gen' = S.union (S.of_list src) gen in
            ((gen', kill), (fgen, fkill))
    | Instr("nop", Some(_), [], None, []) :: rem
    | Label(_) :: rem -> calcAsmGenkill' rem
    | _ -> assert false
let rec calcAsmGenkill b =
    let (igk, fgk) = calcAsmGenkill' b in
    (Ir.var_filter igk, Ir.var_filter fgk)

let array_of_set_equal x y =
    Array.for_all
        (fun x -> x)
        (Array.map2 (fun e1 e2 -> S.equal e1 e2) x y)

let rec calc_lfp gens_kills liveness =
    let ins = fst liveness in
    let outs = snd liveness in
    let ins' = Array.copy ins in
    let outs' = Array.copy outs in
    for i = 0 to (Array.length (fst gens_kills)) - 1 do
        let gen = (fst gens_kills).(i) in
        let kill = (snd gens_kills).(i) in
        ins.(i) <- S.union gen (S.diff outs.(i) kill);
        outs.(i) <- BlockS.fold (fun j ans -> S.union ins.(j) ans) (BlockM.find i graph.adjList) S.empty;
    done;
    if (array_of_set_equal ins ins') && (array_of_set_equal outs outs') then
        ()
    else
        calc_lfp gens_kills liveness

let f bs =
    initGraph bs;
    let n = List.length bs in
    let igens_ikills =
        (Array.init n (fun i -> fst (fst (calcAsmGenkill (List.nth bs i)))),
         Array.init n (fun i -> snd (fst (calcAsmGenkill (List.nth bs i))))) in
    let fgens_fkills =
        (Array.init n (fun i -> fst (snd (calcAsmGenkill (List.nth bs i)))),
         Array.init n (fun i -> snd (snd (calcAsmGenkill (List.nth bs i))))) in
    (* control flow graph *)
    createAsmCtlgraph bs;
    (* initialize in_out array *)
    let iliveness = (Array.make n S.empty, Array.make n S.empty) in
    let fliveness = (Array.make n S.empty, Array.make n S.empty) in
    (* calculate in_out *)
    calc_lfp igens_ikills iliveness;
    calc_lfp fgens_fkills fliveness;
    ((igens_ikills, iliveness), (fgens_fkills, fliveness))

let print_set l = S.iter (fun e -> Printf.printf "%s " e) l

let print liveness =
    Array.iter2
        (fun livein liveout ->
            Printf.printf "in: ";
            print_set livein;
            Printf.printf "\nout: ";
            print_set liveout;
            Printf.printf "\n")
        (fst liveness) (snd liveness)
