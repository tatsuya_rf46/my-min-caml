type t =
    | Label of Id.l
    | Type0 of Asm.t * Asm.t * Asm.t (* (MUnit, IUnit, IUnit) *)
    | Type1 of Asm.t * Asm.t * Asm.t (* (MUnit, MUnit, IUnit) *)
    | Type2 of Asm.t * Asm.t * Asm.t (* (MUnit, FUnit, IUnit) *)
    | Type3 of Asm.t * Asm.t * Asm.t (* (MUnit, MUnit, FUnit) *)
    | Type4 of Asm.t * Asm.t * Asm.t (* (MUnit, IUnit, BUnit) *)
    | Type5 of Asm.t * Asm.t * Asm.t (* (MUnit, FUnit, FUnit) *)
    | Type6 of Asm.t * Asm.t * Asm.t (* (MUnit, MUnit, BUnit) *)
    | Type7 of Asm.t * Asm.t * Asm.t (* (MUnit, FUnit, BUnit) *)
    | Type8 of Asm.t                 (* (PUnit, nop, nop) *)
type u = Nop | MUnit | IUnit | FUnit | BUnit | PUnit (* Unit Type *)
let printUnit = function
    | Nop -> Printf.printf "nop\n"
    | MUnit -> Printf.printf "m-unit\n"
    | IUnit -> Printf.printf "i-unit\n"
    | FUnit -> Printf.printf "f-unit\n"
    | BUnit -> Printf.printf "b-unit\n"
    | PUnit -> Printf.printf "p-unit\n"

let nop = Asm.Instr("nop", Some("dummy"), [], None, [])

let judgeUnit = function
    | Asm.Instr("nop", Some(_), [], None, []) -> Nop
    | Asm.Instr("add", Some(_), [_; _], None, [])
    | Asm.Instr("addi", Some(_), [_], Some(_), [])
    | Asm.Instr("sub", Some(_), [_; _], None, [])
    | Asm.Instr("sll", Some(_), [_], Some(_), [])
    | Asm.Instr("srl", Some(_), [_], Some(_), [])
    | Asm.Instr("li", Some(_), [], Some(_), [])
    | Asm.Instr("move", Some(_), [_], None, [])
    | Asm.Instr("slt", Some(_), [_; _], None, [])
    | Asm.Instr("slti", Some(_), [_], Some(_), []) -> IUnit
    | Asm.Instr("la", Some(_), [], None, [_])
    | Asm.Instr("lw", Some(_), [_], Some(_), [])
    | Asm.Instr("sw", None, [_; _], Some(_), [])
    | Asm.Instr("lw.s", Some(_), [_], Some(_), [])
    | Asm.Instr("lw.s", Some(_), [], None, [_])
    | Asm.Instr("sw.s", None, [_; _], Some(_), [])
    | Asm.Instr("cadd", None, [_; _], None, []) -> MUnit
    | Asm.Instr("add.s", Some(_), [_; _], None, [])
    | Asm.Instr("sub.s", Some(_), [_; _], None, [])
    | Asm.Instr("mul.s", Some(_), [_; _], None, [])
    | Asm.Instr("div.s", Some(_), [_; _], None, [])
    | Asm.Instr("mov.s", Some(_), [_], None, [])
    | Asm.Instr("neg.s", Some(_), [_], None, [])
    | Asm.Instr("ftoi", Some(_), [_], None, [])
    | Asm.Instr("itof", Some(_), [_], None, [])
    | Asm.Instr("abs.s", Some(_), [_], None, [])
    | Asm.Instr("sqrt.s", Some(_), [_], None, [])
    | Asm.Instr("c.eq.s", None, [_; _], Some(0), [])
    | Asm.Instr("c.lt.s", None, [_; _], Some(0), []) -> FUnit
    | Asm.Instr("jal", _, [_; _], None, [_])
    | Asm.Instr("jr", None, [_; _], None, [])
    | Asm.Instr("j", None, _, None, [_])
    | Asm.Instr("beq", None, [_; _], None, [_; _])
    | Asm.Instr("bne", None, [_; _], None, [_; _])
    | Asm.Instr("bf.s", None, [], Some(0), [_; _]) -> BUnit
    | Asm.Instr("fork", None, [_], Some(_), [])
    | Asm.Instr("next", Some(_), [_], None, [])
    | Asm.Instr("end", None, [], None, []) -> PUnit
    | _ -> assert false

let bundleAllocBF i0 i1 i2 =
    match judgeUnit i2 with
    | MUnit | IUnit | Nop -> Type7(i2, i1, i0)
    | _ -> assert false
let bundleAllocBM i0 i1 i2 =
    match judgeUnit i2 with
    | MUnit | IUnit | Nop -> Type6(i2, i1, i0)
    | _ -> assert false
let bundleAllocBI i0 i1 i2 =
    match judgeUnit i2 with
    | IUnit | Nop -> Type4(i2, i1, i0)
    | _ -> assert false
let bundleAllocB i0 i1 i2 =
    match judgeUnit i1, judgeUnit i2 with
    | FUnit, _ -> bundleAllocBF i0 i1 i2
    | _, FUnit -> bundleAllocBF i0 i2 i1
    | MUnit, _ -> bundleAllocBM i0 i1 i2
    | _, MUnit -> bundleAllocBM i0 i2 i1
    | IUnit, _ -> bundleAllocBI i0 i1 i2
    | _, IUnit -> bundleAllocBI i0 i2 i1
    | Nop, Nop -> Type4(nop, nop, i0)
    | _, _     -> assert false
let bundleAllocFF i0 i1 i2 =
    match judgeUnit i2 with
    | MUnit | IUnit | Nop -> Type5(i2, i0, i1)
    | _ -> assert false
let bundleAllocFM i0 i1 i2 =
    match judgeUnit i2 with
    | MUnit | IUnit | Nop -> Type3(i1, i2, i0)
    | _ -> assert false
let bundleAllocF i0 i1 i2 =
    match judgeUnit i1, judgeUnit i2 with
    | FUnit, _ -> bundleAllocFF i0 i1 i2
    | _, FUnit -> bundleAllocFF i0 i2 i1
    | MUnit, _
    | IUnit, _ -> bundleAllocFM i0 i1 i2
    | _, MUnit
    | _, IUnit -> bundleAllocFM i0 i2 i1
    | Nop, Nop -> Type3(nop, nop, i0)
    | _, _     -> assert false
let bundleAllocMM i0 i1 i2 =
    match judgeUnit i2 with
    | IUnit | Nop -> Type1(i0, i1, i2)
    | _ -> assert false
let bundleAllocMI i0 i1 i2 =
    match judgeUnit i2 with
    | IUnit | Nop -> Type0(i0, i1, i2)
    | _ -> assert false
let bundleAllocM i0 i1 i2 =
    match judgeUnit i1, judgeUnit i2 with
    | MUnit, _ -> bundleAllocMM i0 i1 i2
    | _, MUnit -> bundleAllocMM i0 i2 i1
    | IUnit, _ -> bundleAllocMI i0 i1 i2
    | _, IUnit -> bundleAllocMI i0 i2 i1
    | Nop, Nop -> Type0(i0, nop, nop)
    | _, _     -> assert false
let bundleAllocI i0 i1 i2 =
    match judgeUnit i1, judgeUnit i2 with
    | IUnit, IUnit
    | Nop, IUnit
    | IUnit, Nop
    | Nop, Nop     -> Type0(i0, i1, i2)
    | _, _ -> assert false
let bundleAlloc i0 i1 i2 =
    match judgeUnit i0, judgeUnit i1, judgeUnit i2 with
    | PUnit, Nop, Nop -> Type8(i0)
    | BUnit, _, _ -> bundleAllocB i0 i1 i2
    | _, BUnit, _ -> bundleAllocB i1 i0 i2
    | _, _, BUnit -> bundleAllocB i2 i0 i1
    | FUnit, _, _ -> bundleAllocF i0 i1 i2
    | _, FUnit, _ -> bundleAllocF i1 i0 i2
    | _, _, FUnit -> bundleAllocF i2 i0 i1
    | MUnit, _, _ -> bundleAllocM i0 i1 i2
    | _, MUnit, _ -> bundleAllocM i1 i0 i2
    | _, _, MUnit -> bundleAllocM i2 i0 i1
    | IUnit, _, _ -> bundleAllocI i0 i1 i2
    | _, IUnit, _ -> bundleAllocI i1 i0 i2
    | _, _, IUnit -> bundleAllocI i2 i0 i1
    | t0, t1, t2 ->
            printUnit t0; printUnit t1; printUnit t2;
            assert false

let allocatable i0 i1 i2 =
    match Schedule.dependent i0 i1, Schedule.dependent i0 i2, Schedule.dependent i1 i2 with
    | None, None, None ->
            (try let _ = bundleAlloc i0 i1 i2 in true with Assert_failure(_) -> false)
    | _, _, _ -> false

let rec allocate = function
    | [] -> []
    | Asm.Label(l) :: rem -> Label(l) :: allocate rem
    | i0 :: i1 :: i2 :: rem when allocatable i0 i1 i2 ->
            (bundleAlloc i0 i1 i2) :: allocate rem
    | i0 :: i1 :: rem when allocatable i0 i1 nop ->
            (bundleAlloc i0 i1 nop) :: allocate rem
    | i :: rem -> (bundleAlloc i nop nop) :: allocate rem

let f (data, funcodes, is) =
    Format.eprintf "vliw allocation...@.";
    let funcodes = List.map allocate funcodes in
    let is = allocate is in
    (data, funcodes, is)

(*
 Print Functions
 *)
