RESULT = my-min-caml
NCSUFFIX = .opt
CC = gcc
CFLAGS = -g -O2 -Wall
OCAMLLDFLAGS = -warn-error -31
# OCAMLFLAGS += -bin-annot

default: debug-code top $(RESULT)
$(RESULT): debug-code top
clean:: nobackup

SOURCES = type.ml id.ml m.ml s.ml \
		  syntax.ml yacc.mly lex.mll typing.ml \
		  kNormal.ml alpha.ml \
		  kInline.ml \
		  ir.ml loop.ml virtual.ml block.ml \
		  asm.ml \
		  ctlFlowGraph.ml \
		  reachDef.ml \
		  liveAnaly.ml constPropagate.ml \
		  copyPropagate.ml deadElim.ml detectFor.ml \
		  inline.ml \
		  schedule.ml \
		  regAlloc.ml \
		  vliw.ml \
		  float.c emit.ml emit2.ml \
		  main.ml

-include OCamlMakefile
