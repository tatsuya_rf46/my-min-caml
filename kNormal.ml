type t =
    | Unit
    | Int of int
    | Float of float
    | Neg of Id.t
    | Add of Id.t * Id.t
    | Sub of Id.t * Id.t
    | Mul of Id.t
    | Div of Id.t
    | FNeg of Id.t
    | FAdd of Id.t * Id.t
    | FSub of Id.t * Id.t
    | FMul of Id.t * Id.t
    | FDiv of Id.t * Id.t
    | IfEq of Id.t * Id.t * t * t
    | IfLT of Id.t * Id.t * t * t
    | Let of (Id.t * Type.t) * t * t
    | Var of Id.t
    | App of Id.t * Id.t list
    | Tuple of Id.t list
    | LetTuple of (Id.t * Type.t) list * Id.t * t
    | Get of Id.t * Id.t
    | Put of Id.t * Id.t * Id.t
    | ExtArray of Id.t
    | ExtFunApp of Id.t * Id.t list
    (* x.(y) := x.(y) +. z *)
    | CAdd of Id.t * Id.t * Id.t
and fundef = { name : Id.t * Type.t; args : (Id.t * Type.t) list; body : t }

let rec fv = function (* free variables set *)
    | Unit | Int(_) | Float(_) | ExtArray(_) -> S.empty
    | Neg(x) | Mul(x) | Div(x) | FNeg(x) -> S.singleton x
    | Add(x, y) | Sub(x, y) | FAdd(x, y) | FSub(x, y) | FMul(x, y) | FDiv(x, y) | Get(x, y) -> S.of_list [x; y]
    | IfEq(x, y, e1, e2) | IfLT(x, y, e1, e2) -> S.add x (S.add y (S.union (fv e1) (fv e2)))
    | Let((x, t), e1, e2) -> S.union (fv e1) (S.remove x (fv e2))
    | Var(x) -> S.singleton x
    | App(x, ys) -> S.of_list (x :: ys)
    | Tuple(xs) | ExtFunApp(_, xs) -> S.of_list xs
    | Put(x, y, z) -> S.of_list [x; y; z]
    | LetTuple(xs, y, e) -> S.add y (S.diff (fv e) (S.of_list (List.map fst xs)))
    | CAdd(x, y, z) -> S.of_list [x; y; z]

let insert_let (e, t) k =
    match e with
    | Var(x) -> k x
    | _ ->
            let x = Id.gentmp t in
            let e', t' = k x in
            Let((x, t), e, e'), t'

let rec g env =
    let dummypos : Syntax.pos = { line = 0; chars = 0 } in
    function
    | Syntax.Unit _ -> Unit, Type.Unit
    | Syntax.Bool(b, _) -> Int(if b then 1 else 0), Type.Int (* translate bool to int *)
    | Syntax.Int(i, _) -> Int(i), Type.Int
    | Syntax.Float(d, _) -> Float(d), Type.Float
    | Syntax.Not(e) -> g env (Syntax.If(e, Syntax.Bool(false, dummypos), Syntax.Bool(true, dummypos)))
    | Syntax.Neg(e) ->
            insert_let (g env e)
                (fun x -> Neg(x), Type.Int)
    | Syntax.Add(e1, e2) ->
            insert_let (g env e1)
                (fun x -> insert_let (g env e2)
                    (fun y -> Add(x, y), Type.Int))
    | Syntax.Sub(e1, e2) ->
            insert_let (g env e1)
                (fun x -> insert_let (g env e2)
                    (fun y -> Sub(x, y), Type.Int))
    | Syntax.Mul(e) ->
            insert_let (g env e)
                (fun x -> Mul(x), Type.Int)
    | Syntax.Div(e) ->
            insert_let (g env e)
                (fun x -> Div(x), Type.Int)
    | Syntax.FNeg(e) ->
            insert_let (g env e)
                (fun x -> FNeg(x), Type.Float)
    | Syntax.FAdd(e1, e2) ->
            insert_let (g env e1)
                (fun x -> insert_let (g env e2)
                    (fun y -> FAdd(x, y), Type.Float))
    | Syntax.FSub(e1, e2) ->
            insert_let (g env e1)
                (fun x -> insert_let (g env e2)
                    (fun y -> FSub(x, y), Type.Float))
    | Syntax.FMul(e1, e2) ->
            insert_let (g env e1)
                (fun x -> insert_let (g env e2)
                    (fun y -> FMul(x, y), Type.Float))
    | Syntax.FDiv(e1, e2) ->
            insert_let (g env e1)
                (fun x -> insert_let (g env e2)
                    (fun y -> FDiv(x, y), Type.Float))
    | Syntax.Eq _ | Syntax.LT _ as cmp ->
            g env (Syntax.If(cmp, Syntax.Bool(true, dummypos), Syntax.Bool(false, dummypos)))
    | Syntax.If(Syntax.Not(e1), e2, e3) -> g env (Syntax.If(e1, e3, e2))
    | Syntax.If(Syntax.Eq(e1, e2), e3, e4) ->
            insert_let (g env e1)
                (fun x -> insert_let (g env e2)
                    (fun y ->
                        let e3', t3 = g env e3 in
                        let e4', t4 = g env e4 in
                        IfEq(x, y , e3', e4'), t3))
    | Syntax.If(Syntax.LT(e1, e2), e3, e4) ->
            insert_let (g env e1)
                (fun x -> insert_let (g env e2)
                    (fun y ->
                        let e3', t3 = g env e3 in
                        let e4', t4 = g env e4 in
                        IfLT(x, y, e3', e4'), t3))
    | Syntax.If(e1, e2, e3) -> g env (Syntax.If(Syntax.Eq(e1, Syntax.Bool(false, dummypos)), e3, e2))
    | Syntax.Let((x, t), e1, e2) ->
            let e1', t1 = g env e1 in
            let e2', t2 = g (M.add x t env) e2 in
            Let((x, t), e1', e2'), t2
    | Syntax.Var(x, _) when M.mem x env -> Var(x), M.find x env
    | Syntax.Var(x, _) ->
            (match M.find x !Typing.extenv with
            | Type.Array(_) as t -> ExtArray x, t
            | Type.Tuple(_) as t -> ExtArray x, t
            | t -> Type.print t; failwith (Printf.sprintf "external variable %s does not have an array type" x))
    | Syntax.App(Syntax.Var(f, _), e2s) when not (M.mem f env) -> (* external function *)
            (match M.find f !Typing.extenv with
            | Type.Fun(_, t) ->
                    let rec bind xs = function
                        | [] -> ExtFunApp(f, xs), t
                        | e2 :: e2s ->
                                insert_let (g env e2)
                                    (fun x -> bind (xs @ [x]) e2s) in
                    bind [] e2s
            | _ -> assert false)
    | Syntax.App(e1, e2s) ->
            (match g env e1 with
            | _, Type.Fun(_, t) as g_e1 ->
                    insert_let g_e1
                        (fun f ->
                            let rec bind xs = function
                                | [] -> App(f, xs), t
                                | e2 :: e2s ->
                                        insert_let (g env e2)
                                            (fun x -> bind (xs @ [x]) e2s) in
                            bind [] e2s)
            | _ -> assert false)
    | Syntax.Tuple(es) ->
            let rec bind xs ts = function
                | [] -> Tuple(xs), Type.Tuple(ts)
                | e :: es ->
                        let _, t as g_e = g env e in
                        insert_let g_e
                            (fun x -> bind (xs @ [x]) (ts @ [t]) es) in
            bind [] [] es
    | Syntax.LetTuple(xts, e1, e2) ->
            insert_let (g env e1)
                (fun y ->
                    let e2', t2 = g (M.add_list xts env) e2 in
                    LetTuple(xts, y, e2'), t2)
    | Syntax.Array(e1, e2) ->
            insert_let (g env e1)
                (fun x ->
                    let _, t2 as g_e2 = g env e2 in
                    insert_let g_e2
                        (fun y ->
                            let l =
                                match t2 with
                                | Type.Float -> "create_float_array"
                                | _ -> "create_array" in
                            ExtFunApp(l, [x; y]), Type.Array(t2)))
    | Syntax.Get(e1, e2) ->
            (match g env e1 with
            | _, Type.Array(t) as g_e1 ->
                    insert_let g_e1
                        (fun x -> insert_let (g env e2)
                            (fun y -> Get(x, y), t))
            | _ -> assert false)
    | Syntax.Put(e1, e2, e3) ->
            match e3 with
            (* TODO: Only Floating ADD, but now allowing Int ADD *)
            | FAdd(Get(e1', e2'), e3') when Syntax.equal(e1, e1') && Syntax.equal(e2, e2') ->
                    insert_let (g env e1')
                        (fun x -> insert_let (g env e2')
                            (fun y -> insert_let (g env e3')
                                (fun z -> CAdd(x, y ,z), Type.Unit)))
            | _ ->
                    insert_let (g env e1)
                        (fun x -> insert_let (g env e2)
                            (fun y -> insert_let (g env e3)
                                (fun z -> Put(x, y ,z), Type.Unit)))

let rec h env (fundefs, e) =
    match fundefs with
    | { Syntax.name = ((x, t), _); args = yts; body = e1 } :: rem ->
            let env' = M.add x t env in
            let (rem', e'), t2 = h env' (rem, e) in
            let e1', t1 = g (M.add_list yts env') e1 in
            ({ name = (x, t); args = yts; body = e1' } :: rem', e'), t2
    | [] ->
            let e', t = g env e in
            ([], e'), t

let f (fundefs, e) = fst (h M.empty (fundefs, e))

let print_indent chan depth =
    for i = 1 to depth
    do
        Printf.fprintf chan "  "
    done
let rec print_args chan depth args =
    match args with
    | (id_t, type_t) :: rem ->
        (print_indent chan depth;
         Printf.fprintf chan "%s\n" id_t;
         print_args chan depth rem)
    | [] -> ()
let rec print_xs chan depth xs =
    match xs with
    | [] -> ()
    | x :: rem ->
        (print_indent chan depth;
         Printf.fprintf chan "%s\n" x;
         print_xs chan depth rem)
let rec print_exp chan depth = function
    | Unit ->
         print_indent chan depth;
         Printf.fprintf chan "UNIT\n"
    | Int(i) ->
         print_indent chan depth;
         Printf.fprintf chan "INT %d\n" i
    | Float(s) ->
         print_indent chan depth;
         Printf.fprintf chan "FLOAT %f\n" s
    | Neg(x) ->
         print_indent chan depth;
         Printf.fprintf chan "NEG %s\n" x
    | Add(x, y) ->
         print_indent chan depth;
         Printf.fprintf chan "ADD %s %s\n" x y
    | Sub(x, y) ->
         print_indent chan depth;
         Printf.fprintf chan "SUB %s %s\n" x y
    | Mul(x) ->
         print_indent chan depth;
         Printf.fprintf chan "MUL(='<< 4') %s\n" x
    | Div(x) ->
         print_indent chan depth;
         Printf.fprintf chan "DIV(='> 2') %s\n" x
    | FNeg(x) ->
         print_indent chan depth;
         Printf.fprintf chan "FNeg %s\n" x
    | FAdd(x, y) ->
         print_indent chan depth;
         Printf.fprintf chan "FADD %s %s\n" x y
    | FSub(x, y) ->
         print_indent chan depth;
         Printf.fprintf chan "FSub %s %s\n" x y
    | FMul(x, y) ->
         print_indent chan depth;
         Printf.fprintf chan "FMul %s %s\n" x y
    | FDiv(x, y) ->
         print_indent chan depth;
         Printf.fprintf chan "FDiv %s %s\n" x y
    | IfEq(x, y, e1, e2) ->
         print_indent chan depth;
         Printf.fprintf chan "IFEQ %s %s\n" x y;
         print_indent chan depth;
         Printf.fprintf chan "THEN\n";
         print_exp chan (depth+1) e1;
         print_indent chan depth;
         Printf.fprintf chan "ELSE\n";
         print_exp chan (depth+1) e2
    | IfLT(x, y, e1, e2) ->
         print_indent chan depth;
         Printf.fprintf chan "IFLT %s %s\n" x y;
         print_indent chan depth;
         Printf.fprintf chan "THEN\n";
         print_exp chan (depth+1) e1;
         print_indent chan depth;
         Printf.fprintf chan "ELSE\n";
         print_exp chan (depth+1) e2
    | Let((x, t), e1, e2) ->
         print_indent chan depth;
         Printf.fprintf chan "LET %s\n" x;
         print_exp chan (depth+1) e1;
         print_exp chan depth e2
    | Var(x) ->
         print_indent chan depth;
         Printf.fprintf chan "VAR %s\n" x
    | App(x, ys) ->
         print_indent chan depth;
         Printf.fprintf chan "APP %s\n" x;
         print_xs chan (depth+1) ys
    | Tuple(xs) ->
         print_indent chan depth;
         Printf.fprintf chan "TUPLE\n";
         print_xs chan (depth+1) xs
    | LetTuple(xs, y, e) ->
         print_indent chan depth;
         Printf.fprintf chan "LET\n";
         print_indent chan (depth+1);
         Printf.fprintf chan "TUPLE\n";
         print_args chan (depth+2) xs;
         print_indent chan (depth+1);
         Printf.fprintf chan "%s\n" y;
         print_exp chan depth e
    | Get(x, y) ->
         print_indent chan depth;
         Printf.fprintf chan "GET %s %s\n" x y
    | Put(x, y, z) ->
         print_indent chan depth;
         Printf.fprintf chan "PUT %s %s %s\n" x y z
    | ExtArray(x) ->
            print_indent chan depth;
            Printf.fprintf chan "EXTARRAY %s\n" x
    | ExtFunApp(x, ys) ->
            print_indent chan depth;
            Printf.fprintf chan "EXTFUNAPP %s\n" x;
            print_indent chan (depth+1);
            Printf.fprintf chan "ARGS\n";
            print_xs chan (depth+2) ys
    | CAdd(x, y, z) ->
         print_indent chan depth;
         Printf.fprintf chan "CADD %s.(%s) %s\n" x y z
and print_fundef chan depth f =
    let (id_t, type_t) = f.name in
    Printf.fprintf chan "LETREC %s\n" id_t;
    print_indent chan depth;
    Printf.fprintf chan "ARGS\n";
    print_args chan (depth+1) f.args;
    print_indent chan depth;
    Printf.fprintf chan "BODY\n";
    print_exp chan (depth+1) f.body
let print chan (fundefs, e) =
    List.iter (print_fundef chan 1) fundefs;
    Printf.fprintf chan "MAIN\n";
    print_exp chan 0 e;
    (fundefs, e)
