(* put global arrays in the data section *)
open KNormal

let data = ref []

let rec remove = function (* remove data except global arrays *)
    | (_, Data.Int(_)) :: rem | (_, Data.Float(_)) :: rem | (_, Data.Array(_)) :: rem ->
            remove rem
    | datum :: rem ->
            datum :: (remove rem)
    | [] -> []

exception Error of string
let rec h env = function
    | Int(i) -> Data.Int(i)
    | Float(s) -> Data.Float(s)
    | ExtFunApp(x, ys) when x = "create_array" || x = "create_float_array" ->
            let l = Id.L(Id.genid "glb_array") in
            assert ((List.length ys) = 2);
            (match (M.find (List.hd ys) env, M.find (List.nth ys 1) env) with
            | Data.Int(i1), Data.Int(i2) ->
                    data:= (l, Data.ArrayOfInt(i1, i2)) :: !data;
                    Data.Array(l)
            | Data.Int(i), Data.Float(s) ->
                    data:= (l, Data.ArrayOfFloat(i, s)) :: !data;
                    Data.Array(l)
            | Data.Int(i), Data.Array(a) ->
                    data:= (l, Data.ArrayOfArray(i, a)) :: !data;
                    Data.Array(l)
            | _ -> assert false)
    | Let((x, t), e1, e2) ->
            let v = h env e1 in
            h (M.add x v env) e2
    | _ -> raise (Error "a global variable is not constant")
let rec g fundefs = function (* global *)
    | Let((x, t), e1, e2) ->
            data := (Id.L(x), h M.empty e1) :: !data;
            g fundefs e2
    | LetRec(fundef, e) ->
            g (fundef :: fundefs) e
    | Main(e) -> (fundefs, e)
    | _ -> assert false
let f e =
    data := [];
    let (fundefs, e) = g [] e in
    data := remove !data;
    Prog(!data, fundefs, e)
