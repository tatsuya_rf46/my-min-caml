open Ir

(* floating points table *)
let data = ref []

let classify xts ini addf addi =
    List.fold_left
        (fun acc (x, t) ->
            match t with
            | Type.Unit -> acc
            | Type.Float -> addf acc x
            | _ -> addi acc x t)
        ini
        xts

let separate xts =
    classify
        xts
        ([], [])
        (fun (int, float) x -> (int, float @ [x]))
        (fun (int, float) x _ -> (int @ [x], float))

let expand xts ini addf addi =
    classify
        xts
        ini
        (fun (offset, acc) x ->
            (offset + 4, addf x offset acc))
        (fun (offset, acc) x t ->
            (offset + 4, addi x t offset acc))

let rec g env = function (* convert to virtual codes *)
    | KNormal.Unit -> [Ans(Nop)]
    | KNormal.Int(i) -> [Ans(Li(i))]
    | KNormal.Float(s) ->
            let l =
                try
                    (* reuse if floating point already exists *)
                    let (l, _) = List.find (fun (_, datum) -> s = datum) !data in
                    l
                with Not_found ->
                    let l = Id.L(Id.genid "float") in
                    data := (l, s) :: !data;
                    l
            in
            [Ans(FLw(0, L(l)))]
    | KNormal.Neg(x) -> [Ans(Sub(reg_zero, x))]
    | KNormal.Add(x, y) -> [Ans(Add(x, V(y)))]
    | KNormal.Sub(x, y) -> [Ans(Sub(x, y))]
    | KNormal.Mul(x) -> [Ans(Mul(x))]
    | KNormal.Div(x) -> [Ans(Div(x))]
    | KNormal.FNeg(x) -> [Ans(FNeg(x))]
    | KNormal.FAdd(x, y) -> [Ans(FAdd(x, y))]
    | KNormal.FSub(x, y) -> [Ans(FSub(x, y))]
    | KNormal.FMul(x, y) -> [Ans(FMul(x, y))]
    | KNormal.FDiv(x, y) -> [Ans(FDiv(x, y))]
    | KNormal.CAdd(x, y, z) ->
            let address = Id.genid x in
            let quad = Id.genid y in
            (match M.find x env with
            | Type.Array(Type.Float) ->
                    (* Byte addressing: make y quad *)
                    Let((quad, Type.Int), Mul(y)) ::
                        Let((address, Type.Int), Add(x, V(quad))) ::
                            [Ans(CAdd(z, address))]
            | _ -> assert false)
    | KNormal.ExtFunApp(x, ys) ->
            if x = Id.string_to_id "int_of_float" then
                (assert (List.length ys = 1);
                 [Ans(Ftoi(List.hd ys))])
            else if x = Id.string_to_id "float_of_int" then
                (assert (List.length ys = 1);
                 [Ans(Itof(List.hd ys))])
            else if x = Id.string_to_id "fabs" then
                (assert (List.length ys = 1);
                 [Ans(FAbs(List.hd ys))])
            else if x = Id.string_to_id "abs_float" then
                (assert (List.length ys = 1);
                 [Ans(FAbs(List.hd ys))])
            else if x = Id.string_to_id "sqrt" then
                (assert (List.length ys = 1);
                 [Ans(FSqrt(List.hd ys))])
            else
            let (int, float) = separate (List.map (fun y -> (y, M.find y env)) ys) in
            [Ans(Call(Id.L("min_caml_" ^ x), int, float))]
    | KNormal.IfEq(x, y, e1, e2) ->
            (match M.find x env with
            | Type.Bool | Type.Int ->
                    let then_l = Id.L(Id.genid "ifeq_then") in
                    let else_l = Id.L(Id.genid "ifeq_else") in
                    IfEq(x, y, then_l, else_l) ::
                        (Label(then_l) :: g env e1 @
                            (Label(else_l) :: g env e2))
            | Type.Float ->
                    let then_l = Id.L(Id.genid "iffeq_then") in
                    let else_l = Id.L(Id.genid "iffeq_else") in
                    IfFEq(x, y, then_l, else_l) ::
                        (Label(then_l) :: g env e1 @
                            (Label(else_l) :: g env e2))
            | _ -> failwith "equality supported only for bool, int, and float")
    | KNormal.IfLT(x, y, e1, e2) ->
            (match M.find x env with
            | Type.Bool | Type.Int ->
                    let then_l = Id.L(Id.genid "iflt_then") in
                    let else_l = Id.L(Id.genid "iflt_else") in
                    IfLT(x, V(y), then_l, else_l) ::
                        (Label(then_l) :: g env e1 @
                            (Label(else_l) :: g env e2))
            | Type.Float ->
                    let then_l = Id.L(Id.genid "ifflt_then") in
                    let else_l = Id.L(Id.genid "ifflt_else") in
                    IfFLT(x, y, then_l, else_l) ::
                        (Label(then_l) :: g env e1 @
                            (Label(else_l) :: g env e2))
            | _ -> failwith "inequality supported only for bool, int, and float")
    | KNormal.Let((x, t1), e1, e2) ->
            let e1' = g env e1 in
            let e2' = g (M.add x t1 env) e2 in
            (bind (x, t1) e1') @ e2'
    | KNormal.Var(x) ->
            (match M.find x env with
            | Type.Unit -> [Ans(Nop)]
            | Type.Float -> [Ans(FMov(x))]
            | _ -> [Ans(Mov(x))])
    | KNormal.App(x, ys) ->
            let (int, float) = separate (List.map (fun y -> (y, M.find y env)) ys) in
            [Ans(Call(Id.L(x), int, float))]
    | KNormal.Tuple(xs) ->
            let y = Id.genid "tuple" in
            let (offset, store) =
                expand
                    (List.map (fun x -> (x, M.find x env)) xs)
                    (0, [Ans(Mov(y))])
                    (fun x offset store -> Let((Id.gentmp Type.Unit, Type.Unit), FSw(x, offset, y)) :: store)
                    (fun x _ offset store -> Let((Id.gentmp Type.Unit, Type.Unit), Sw(x, offset, y)) :: store)
                in
            Let((y, Type.Tuple(List.map (fun x -> M.find x env) xs)), Add(reg_gp, C(0))) ::
                Let((reg_gp, Type.Int), Add(reg_gp, C(offset))) ::
                    store
    | KNormal.LetTuple(xts, y, e2) ->
            let s = KNormal.fv e2 in
            let (offset, load) =
                expand
                    xts
                    (0, g (M.add_list xts env) e2)
                    (fun x offset load ->
                        if not (S.mem x s) then load else
                            Let((x, Type.Float), FLw(offset, V(y))) :: load)
                    (fun x t offset load ->
                        if not (S.mem x s) then load else
                            Let((x, t), Lw(offset, y)) :: load) in
            load
    | KNormal.Get(x, y) ->
            let address = Id.genid x in
            let quad = Id.genid y in
            (match M.find x env with
            | Type.Array(Type.Unit) -> [Ans(Nop)]
            | Type.Array(Type.Float) ->
                    (* Byte addressing: make y quad *)
                    Let((quad, Type.Int), Mul(y)) ::
                        Let((address, Type.Int), Add(x, V(quad))) ::
                            [Ans(FLw(0, V(address)))]
            | Type.Array(_) ->
                    (* Byte addressing: make y quad *)
                    Let((quad, Type.Int), Mul(y)) ::
                        Let((address, Type.Int), Add(x, V(quad))) ::
                            [Ans(Lw(0, address))]
            | _ -> assert false)
    | KNormal.Put(x, y, z) ->
            let address = Id.genid x in
            let quad = Id.genid y in
            (match M.find x env with
            | Type.Array(Type.Unit) -> [Ans(Nop)]
            | Type.Array(Type.Float) ->
                    (* Byte addressing: make y quad *)
                    Let((quad, Type.Int), Mul(y)) ::
                        Let((address, Type.Int), Add(x, V(quad))) ::
                            [Ans(FSw(z, 0, address))]
            | Type.Array(_) ->
                    (* Byte addressing: make y quad *)
                    Let((quad, Type.Int), Mul(y)) ::
                        Let((address, Type.Int), Add(x, V(quad))) ::
                            [Ans(Sw(z, 0, address))]
            | _ -> assert false)
    | KNormal.ExtArray(x) -> [Ans(La(Id.L("min_caml_" ^ x)))]

let prologue int float =
    let (reg_save, saved_vars, saved_fvars) =
        List.fold_left
            (fun (e, saved_vars, saved_fvars) x ->
                let saved_var = Id.gentmp Type.Int in
                (Let((saved_var, Type.Int), Mov(x)) :: e, saved_var :: saved_vars, saved_fvars))
            (List.fold_left
                (fun (e, saved_vars, saved_fvars) x ->
                    let saved_var = Id.gentmp Type.Float in
                    (Let((saved_var, Type.Float), FMov(x)) :: e, saved_vars, saved_var :: saved_fvars))
                ([], [], [])
                (Array.to_list Ir.callee_fregs))
            (Array.to_list Ir.callee_regs) in
    (reg_save, List.rev saved_vars, List.rev saved_fvars)
let epilogue saved_vars saved_fvars =
    List.fold_left2
        (fun e r x -> Let((r, Type.Int), Mov(x)) :: e)
        (List.fold_left2
            (fun e r x -> Let((r, Type.Float), FMov(x)) :: e)
            []
            (Array.to_list Ir.callee_fregs)
            saved_fvars)
        (Array.to_list Ir.callee_regs)
        saved_vars
let rec insertEpilogue l epilogue = function
    | [] -> []
    | Ans(exp) :: rem ->
            let Id.L(s) = l in
            let l_end = Id.L((Id.genid s) ^ "_end") in
            Jump(l_end) :: Label(l_end) :: epilogue @ (Ans(exp) :: insertEpilogue l epilogue rem)
    | e :: rem -> e :: insertEpilogue l epilogue rem

let h { KNormal.name = (x, t);
        KNormal.args = yts;
        KNormal.body = e } =
    let (int, float) = separate yts in
    let l_start = Id.L(x ^ "_start") in
    let (save, saved_vars, saved_fvars) = prologue int float in
    let e' = g (M.add x t (M.add_list yts M.empty)) e in
    let e' =
        Label(Id.L(x)) ::
            save @
                (Jump(l_start) ::
                    Label(l_start) :: 
                        insertEpilogue (Id.L(x)) (epilogue saved_vars saved_fvars) e') in
    match t with
    | Type.Fun(_, t2) ->
            { name = Id.L(x); args = int; fargs = float; body = e'; ret = t2 }
    | _ -> Printf.printf "%s" x; Type.print t; assert false

let f (fundefs, e) =
    data := [];
    let fundefs = List.map h fundefs in
    let e = Label(Id.L("min_caml_start")) :: g M.empty e in
    (!data, fundefs, e)
