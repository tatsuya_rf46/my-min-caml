open Asm

let compare_double (x0, y0) (x1, y1) =
    match Stdlib.compare x0 x1 with
    | 0 -> Stdlib.compare y0 y1
    | c -> c

module MoveSet =
    Set.Make
        (struct
            type t = Asm.t
            let compare i0 i1 =
                match i0, i1 with
                | Instr("move", Some(x0), [y0], None, []), Instr("move", Some(x1), [y1], None, [])
                | Instr("mov.s", Some(x0), [y0], None, []), Instr("mov.s", Some(x1), [y1], None, []) ->
                        compare_double (x0, y0) (x1, y1)
                | _ -> assert false
        end)
module AdjSet =
    Set.Make
        (struct
            type t = Id.t * Id.t
            let compare = compare_double
        end)

type vertexData = {
    mutable precolored : S.t;
    mutable initial : S.t;
    mutable simplifyWorklist : S.t;
    mutable freezeWorklist : S.t;
    mutable spillWorklist : S.t;
    mutable spilledNodes : S.t;
    mutable coalescedNodes : S.t;
    mutable coloredNodes : S.t;
    mutable selectStack : Id.t Stack.t
}
type movesData = {
    mutable coalescedMoves : MoveSet.t;
    mutable constrainedMoves : MoveSet.t;
    mutable frozenMoves : MoveSet.t;
    mutable worklistMoves : MoveSet.t;
    mutable activeMoves : MoveSet.t
}
type otherData = {
    mutable adjSet : AdjSet.t;
    mutable adjList : S.t M.t;
    mutable degree : int M.t;
    mutable moveList : MoveSet.t M.t;
    mutable alias : Id.t M.t;
    mutable color : Id.t M.t;
    mutable regs : S.t
}

let vertexData = {
    precolored = S.empty;
    initial = S.empty;
    simplifyWorklist = S.empty;
    freezeWorklist = S.empty;
    spillWorklist = S.empty;
    spilledNodes = S.empty;
    coalescedNodes = S.empty;
    coloredNodes = S.empty;
    selectStack = Stack.create ()
}
let movesData = {
    coalescedMoves = MoveSet.empty;
    constrainedMoves = MoveSet.empty;
    frozenMoves = MoveSet.empty;
    worklistMoves = MoveSet.empty;
    activeMoves = MoveSet.empty
}
let otherData = {
    adjSet = AdjSet.empty;
    adjList = M.empty;
    degree = M.empty;
    moveList = M.empty;
    alias = M.empty;
    color = M.empty;
    regs = S.empty
}

let intk = List.length Ir.allregs
let floatk = List.length Ir.allfregs

let select_type (ivalue, fvalue) = function
    | Type.Float -> fvalue
    | _ -> ivalue

(* variables set in blocks *)
let blocks_vars bs =
    List.fold_left
        (fun s b ->
            List.fold_left
                (fun (s1, s2) instr ->
                    let (vars, fvars) = instr_vars instr in
                    (S.union vars s1, S.union fvars s2))
                s
                b)
        (S.empty, S.empty)
        bs

let dataInit t bs =
    let vars = S.filter (fun x -> Ir.not_reg x) (select_type (blocks_vars bs) t) in
    vertexData.precolored <- select_type (S.of_list Ir.allregs, S.of_list Ir.allfregs) t;
    vertexData.initial <- vars;
    vertexData.simplifyWorklist <- S.empty;
    vertexData.freezeWorklist <- S.empty;
    vertexData.spillWorklist <- S.empty;
    vertexData.spilledNodes <- S.empty;
    vertexData.coalescedNodes <- S.empty;
    vertexData.coloredNodes <- S.empty;
    vertexData.selectStack <- Stack.create ();
    movesData.coalescedMoves <- MoveSet.empty;
    movesData.constrainedMoves <- MoveSet.empty;
    movesData.frozenMoves <- MoveSet.empty;
    movesData.worklistMoves <- MoveSet.empty;
    movesData.activeMoves <- MoveSet.empty;
    otherData.adjSet <- AdjSet.empty;
    otherData.adjList <-
        S.fold
            (fun e m -> M.add e S.empty m)
            vars
            M.empty;
    otherData.degree <-
        S.fold
            (fun e m -> M.add e 0 m)
            vars
            M.empty;
    otherData.degree <-
        S.fold
            (fun e m -> M.add e 100000 m) (* infinity *)
            vertexData.precolored (* all regs *)
            otherData.degree;
    otherData.moveList <-
        S.fold
            (fun e m -> M.add e MoveSet.empty m)
            (S.union vars vertexData.precolored)
            M.empty;
    otherData.alias <- M.empty;
    otherData.color <-
        S.fold
            (fun e m -> M.add e e m)
            vertexData.precolored
            M.empty;
    otherData.regs <- S.empty;
    ()

let addEdge u v =
    if (not (AdjSet.mem (u, v) otherData.adjSet)) && (u != v) then
        (otherData.adjSet <- AdjSet.add (u, v) (AdjSet.add (v, u) otherData.adjSet);
         if not (S.mem u vertexData.precolored) then
             (otherData.adjList <- M.add u (S.add v (M.find u otherData.adjList)) otherData.adjList;
              otherData.degree <- M.add u ((M.find u otherData.degree) + 1) otherData.degree);
         if not (S.mem v vertexData.precolored) then
             (otherData.adjList <- M.add v (S.add u (M.find v otherData.adjList)) otherData.adjList;
              otherData.degree <- M.add v ((M.find v otherData.degree) + 1) otherData.degree);
         ())

let build t bs (gens_kills, liveness) =
    List.iteri
        (fun i b ->
            let live = ref (snd liveness).(i) in
            for j = (List.length b) - 1 downto 0 do
                let instr = List.nth b j in
                (* (1* debug *1) Asm.print stdout ([], [], [[instr]]); *)
                let (gen, kill) = select_type (LiveAnaly.calcAsmGenkill [instr]) t in
                if isMove t instr then
                    (live := S.diff !live gen;
                     S.iter
                        (fun x ->
                            otherData.moveList <-
                                M.add x (MoveSet.add instr (M.find x otherData.moveList)) otherData.moveList)
                        (S.union gen kill);
                     movesData.worklistMoves <- MoveSet.add instr movesData.worklistMoves);
                live := S.union !live kill;
                S.iter
                    (fun d ->
                        S.iter
                            (fun l ->
                                if d != l then
                                    (try addEdge l d with Not_found ->
                                        Printf.printf "%s %s\n" l d;
                                        failwith "this10")
                                else
                                    ())
                            !live)
                    kill;
                live := S.union gen (S.diff !live kill)
            done)
        bs

let adjacent v =
    (* (1* debug *1) Printf.printf "find %s: in " v; *)
    (*    M.print otherData.adjList; *)
    (*    let _ = *)
    (*        try M.find v otherData.adjList *)
    (*        with Not_found -> failwith "aaa" in *)
    (*    Printf.printf "\n"; *)
    S.diff
        (M.find v otherData.adjList)
        (S.union (S.of_seq (Stack.to_seq vertexData.selectStack)) vertexData.coalescedNodes)

let nodeMoves v =
    (* (1* debug *1) Printf.printf "find %s: in " v; *)
    (*     M.print otherData.moveList; *)
    (*     Printf.printf "\n"; *)
    MoveSet.inter
        (M.find v otherData.moveList)
        (MoveSet.union movesData.activeMoves movesData.worklistMoves)

let moveRelated v = not (MoveSet.is_empty (nodeMoves v))

let makeWorklist t =
    let k = select_type (intk, floatk) t in
    S.iter
        (fun v ->
            vertexData.initial <- S.remove v vertexData.initial;
            if (M.find v otherData.degree) >= k then
                vertexData.spillWorklist <- S.add v vertexData.spillWorklist
            else if moveRelated v then
                vertexData.freezeWorklist <- S.add v vertexData.freezeWorklist
            else
                vertexData.simplifyWorklist <- S.add v vertexData.simplifyWorklist)
        vertexData.initial

let break_cond () =
    (S.is_empty vertexData.simplifyWorklist) &&
    (MoveSet.is_empty movesData.worklistMoves) &&
    (S.is_empty vertexData.freezeWorklist) &&
    (S.is_empty vertexData. spillWorklist)

let enableMoves ns =
    S.iter
        (fun n ->
            MoveSet.iter
                (fun m ->
                    if MoveSet.mem m movesData.activeMoves then
                        (movesData.activeMoves <- MoveSet.add m movesData.activeMoves;
                         movesData.worklistMoves <- MoveSet.add m movesData.worklistMoves))
                (nodeMoves n))
        ns

let decrementDegree t m =
    let k = select_type (intk, floatk) t in
    (* (1* debug *1) Printf.printf "find %s: in " m; *)
    (*     M.print otherData.degree; *)
    (*     Printf.printf "\n"; *)
    let d = M.find m otherData.degree in
    otherData.degree <- M.add m ((M.find m otherData.degree) - 1) otherData.degree;
    if d = k then
        (enableMoves (S.add m (adjacent m));
         vertexData.spillWorklist <- S.add m vertexData.spillWorklist;
         if moveRelated m then
            vertexData.freezeWorklist <- S.add m vertexData.freezeWorklist
         else
            vertexData.simplifyWorklist <- S.add m vertexData.simplifyWorklist)

let simplify t =
    let n = S.min_elt vertexData.simplifyWorklist in
    vertexData.simplifyWorklist <- S.remove n vertexData.simplifyWorklist;
    Stack.push n vertexData.selectStack;
    S.iter (fun m -> decrementDegree t m) (adjacent n)

let moveVars t = function
    | Instr("move", Some(x), [y], None, []) ->
            if t = Type.Int then
                (ref x, ref y)
            else
                assert false
    | Instr("mov.s", Some(x), [y], None, []) ->
            if t = Type.Float then
                (ref x, ref y)
            else
                assert false
    | _ -> assert false

let rec getAlias n =
    if S.mem n vertexData.coalescedNodes then
        getAlias (M.find n otherData.alias)
    else
        n

let addWorkList t u =
    let k = select_type (intk, floatk) t in
    if (not (S.mem u vertexData.precolored)) &&
       (not (moveRelated u)) &&
       ((M.find u otherData.degree) < k) then
            vertexData.simplifyWorklist <- S.add u vertexData.simplifyWorklist

let ok t p r =
    let k = select_type (intk, floatk) t in
    ((M.find p otherData.degree) < k) ||
    (S.mem p vertexData.precolored) ||
    (AdjSet.mem (p, r) otherData.adjSet)

let conservative t ns =
    let k = select_type (intk, floatk) t in
    let p = ref 0 in
    S.iter
        (fun n ->
            if (M.find n otherData.degree) >= k then p := !p + 1)
        ns;
    !p < k

let combine t u v =
    let k = select_type (intk, floatk) t in
    if S.mem v vertexData.freezeWorklist then
        vertexData.freezeWorklist <- S.remove v vertexData.freezeWorklist
    else
        vertexData.spillWorklist <- S.remove v vertexData.spillWorklist;
    vertexData.coalescedNodes <- S.add v vertexData.coalescedNodes;
    otherData.alias <- M.add v u otherData.alias;
    otherData.moveList <-
        M.add
            u
            (MoveSet.union (M.find u otherData.moveList) (M.find v otherData.moveList))
            otherData.moveList;
    enableMoves (S.singleton v);
    S.iter
        (fun p ->
            addEdge p u;
            decrementDegree t p)
        (adjacent v);
    if ((M.find u otherData.degree) >= k) && (S.mem u vertexData.freezeWorklist) then
        (vertexData.freezeWorklist <- S.remove u vertexData.freezeWorklist;
         vertexData.spillWorklist <- S.add u vertexData.spillWorklist)

let coalesce t =
    let m = MoveSet.min_elt movesData.worklistMoves in
    let (x, y) = moveVars t m in
    x := getAlias !x;
    y := getAlias !y;
    let (u, v) =
        if S.mem !y vertexData.precolored then (!y, !x) else (!x, !y) in
    movesData.worklistMoves <- MoveSet.remove m movesData.worklistMoves;
    if u = v then
        (movesData.coalescedMoves <- MoveSet.add m movesData.coalescedMoves;
         addWorkList t u)
    else if (S.mem v vertexData.precolored) || (AdjSet.mem (u, v) otherData.adjSet) then
        (movesData.constrainedMoves <- MoveSet.add m movesData.constrainedMoves;
         addWorkList t u;
         addWorkList t v)
    else if ((S.mem u vertexData.precolored) &&
             (S.for_all (fun p -> ok t p u) (adjacent v)))
                ||
            ((not (S.mem u vertexData.precolored)) &&
             (conservative t (S.union (adjacent u) (adjacent v)))) then
        (movesData.coalescedMoves <- MoveSet.add m movesData.coalescedMoves;
         combine t u v;
         addWorkList t u)
    else
        movesData.activeMoves <- MoveSet.add m movesData.activeMoves

let freezeMoves t u =
    let k = select_type (intk, floatk) t in
    MoveSet.iter
        (fun m ->
            let (x, y) = moveVars t m in
            let v = if (getAlias !y) = (getAlias u) then getAlias !x else getAlias !y in
            movesData.activeMoves <- MoveSet.remove m movesData.activeMoves;
            movesData.frozenMoves <- MoveSet.add m movesData.frozenMoves;
            if (MoveSet.is_empty (nodeMoves v)) && ((M.find v otherData.degree) < k) then
                (vertexData.freezeWorklist <- S.remove v vertexData.freezeWorklist;
                 vertexData.simplifyWorklist <- S.add v vertexData.simplifyWorklist))
        (nodeMoves u)

let freeze t =
    let u = S.min_elt vertexData.freezeWorklist in
    vertexData.freezeWorklist <- S.remove u vertexData.freezeWorklist;
    vertexData.simplifyWorklist <- S.add u vertexData.simplifyWorklist;
    freezeMoves t u

let rec usedTimes' n = function
    | [] -> 0.0
    | e :: rem ->
            let (vars, fvars) = instr_vars e in
            (if (S.mem n vars) || (S.mem n fvars) then 1.0 else 0.0) +. usedTimes' n rem
let rec usedTimes n = function
    | [] -> 0.0
    | b :: rem -> usedTimes' n b +. usedTimes n rem
(* TODO: consider loop *)
let spillPreference n bs =
    (usedTimes n bs) /. float_of_int (M.find n otherData.degree)
let rec minSpillPreference bs =
    S.fold
        (fun elt n ->
            if (spillPreference elt bs) < (spillPreference n bs) then elt else n)
        vertexData.spillWorklist
        (S.min_elt vertexData.spillWorklist)
let selectSpill t bs =
    let m = minSpillPreference bs in
    vertexData.spillWorklist <- S.remove m vertexData.spillWorklist;
    vertexData.simplifyWorklist <- S.add m vertexData.simplifyWorklist;
    freezeMoves t m

let assignColors t =
    while not (Stack.is_empty vertexData.selectStack) do
        let n = Stack.pop vertexData.selectStack in
        otherData.regs <- S.of_list (select_type (Ir.allregs, Ir.allfregs) t);
        S.iter
            (fun w ->
                if S.mem (getAlias w) (S.union vertexData.coloredNodes vertexData.precolored) then
                    otherData.regs <- S.remove (M.find (getAlias w) otherData.color) otherData.regs;
                )
            (M.find n otherData.adjList);
        if S.is_empty otherData.regs then
            vertexData.spilledNodes <- S.add n vertexData.spilledNodes
        else
            (vertexData.coloredNodes <- S.add n vertexData.coloredNodes;
             let c = S.min_elt otherData.regs in
             otherData.color <- M.add n c otherData.color)
    done;
    S.iter
        (fun n ->
            otherData.color <- M.add n (M.find (getAlias n) otherData.color) otherData.color)
        vertexData.coalescedNodes

(* register allocation, refering to vertexData.color *)
let regAllocVar x = 
    match M.find_opt x otherData.color with
    | Some(r) -> r
    | None -> x
let regAllocOpt = function
    | Some(v) -> Some(regAllocVar v)
    | None -> None
let rec regAllocList = function
    | [] -> []
    | x :: rem -> (regAllocVar x) :: regAllocList rem
let rec regAllocSub' = function
    | Instr(mne, dst, src, imm, label) -> Instr(mne, regAllocOpt dst, regAllocList src, imm, label)
    | Label(l) -> Label(l)
let rec regAllocSub = function
    | [] -> []
    | i :: rem -> (regAllocSub' i) :: regAllocSub rem
let rec regAlloc = function
    | [] -> []
    | b :: rem  -> (regAllocSub b) :: regAlloc rem

let stack_size = ref 0
let rec insertSpillSub' t v = function (* insert spill and replace v with v_new *)
    | [] -> []
    (* Call case *)
    (* | Instr("sw", None, [reg_ra0; reg_sp0], Some(ss0), []) :: *)
    (*         Instr("addi", Some(reg_sp1), [reg_sp2], Some(ss1), []) :: *)
    (*             Instr("jal", dst, [arg_num; farg_num], None, [l]) :: *)
    (*                 Instr("addi", Some(reg_sp3), [reg_sp4], Some(ss2), []) :: *)
    (*                     Instr("lw", Some(reg_ra1), [reg_sp5], Some(ss3), []) :: rem *)
    (*                         when reg_ra0 = Ir.reg_ra && reg_ra1 = Ir.reg_ra && *)
    (*                              reg_sp0 = Ir.reg_sp && reg_sp0 = Ir.reg_sp && *)
    (*                              reg_sp2 = Ir.reg_sp && reg_sp3 = Ir.reg_sp && *)
    (*                              reg_sp4 = Ir.reg_sp && reg_sp5 = Ir.reg_sp && *)
    (*                              ss0 + 4 = ss1 && -ss2 = ss1 && ss3 + 4 = ss1 -> *)
    (*         let ss = !stack_size in *)
    (*         Instr("sw", None, [reg_ra0; reg_sp0], Some(ss), []) :: *)
    (*             Instr("addi", Some(reg_sp1), [reg_sp2], Some(ss+4), []) :: *)
    (*                 Instr("jal", dst, [arg_num; farg_num], None, [l]) :: *)
    (*                     Instr("addi", Some(reg_sp3), [reg_sp4], Some(-(ss+4)), []) :: *)
    (*                         Instr("lw", Some(reg_ra1), [reg_sp5], Some(ss), []) :: *)
    (*                             insertSpillSub' t v rem *)
    | Instr("sw", None, [reg_ra; reg_sp], Some(_), []) :: rem
        when reg_ra = Ir.reg_ra && reg_sp = Ir.reg_sp ->
            let ss = !stack_size in
            Instr("sw", None, [reg_ra; reg_sp], Some(ss), []) :: insertSpillSub' t v rem
    | Instr("addi", Some(reg_sp0), [reg_sp1], Some(ss), []) :: rem
        when reg_sp0 = Ir.reg_sp && reg_sp1 = Ir.reg_sp && ss > 0 ->
            let ss = !stack_size in
            Instr("addi", Some(reg_sp0), [reg_sp1], Some(ss+4), []) :: insertSpillSub' t v rem
    | Instr("addi", Some(reg_sp0), [reg_sp1], Some(ss), []) :: rem
        when reg_sp0 = Ir.reg_sp && reg_sp1 = Ir.reg_sp && ss < 0 ->
            let ss = !stack_size in
            Instr("addi", Some(reg_sp0), [reg_sp1], Some(-(ss+4)), []) :: insertSpillSub' t v rem
    | Instr("lw", Some(reg_ra), [reg_sp], Some(_), []) :: rem
        when reg_ra = Ir.reg_ra && reg_sp = Ir.reg_sp ->
            let ss = !stack_size in
            Instr("lw", Some(reg_ra), [reg_sp], Some(ss), []) :: insertSpillSub' t v rem
    | e :: rem ->
            let (gen, kill) = select_type (LiveAnaly.calcAsmGenkill [e]) t in
            let restore_or_not =
                (if S.mem v gen then
                    match t with
                    | Type.Float -> Instr("lw.s", Some(v), [Ir.reg_sp], Some(!stack_size - 4), []) :: [e]
                    | _ -> Instr("lw", Some(v), [Ir.reg_sp], Some(!stack_size - 4), []) :: [e]
                else
                    [e]) in
            let store_or_not =
                (if S.mem v kill then
                    match t with
                    | Type.Float -> [Instr("sw.s", None, [v; Ir.reg_sp], Some(!stack_size - 4), [])]
                    | _ -> [Instr("sw", None, [v; Ir.reg_sp], Some(!stack_size - 4), [])]
                else
                    []) in
            restore_or_not @ store_or_not @ (insertSpillSub' t v rem)
let rec insertSpillSub t v = function
    | [] -> []
    | b :: rem ->
            insertSpillSub' t v b :: insertSpillSub t v rem
let insertSpill t v bs =
    stack_size := !stack_size + 4;
    insertSpillSub t v bs

let rec rewriteProgram t bs =
    if not (S.is_empty vertexData.spilledNodes) then
        (let v = S.min_elt vertexData.spilledNodes in
        vertexData.spilledNodes <- S.remove v vertexData.spilledNodes;
        insertSpill t v bs)
    else
        bs

let printData () =
    Printf.printf "precolored: ";
    S.print vertexData.precolored;
    Printf.printf "\ninitial: ";
    S.print vertexData.initial;
    Printf.printf "\ncoalescedNodes: ";
    S.iter (fun x -> Printf.printf "%s-%s " x (getAlias x)) vertexData.coalescedNodes;
    Printf.printf "\nedges: ";
    AdjSet.iter
        (fun (u, v) -> Printf.printf "(%s, %s) " u v)
        otherData.adjSet;
    Printf.printf "\ncolor: ";
    M.iter (fun k e -> Printf.printf "%s-%s " k e) otherData.color;
    Printf.printf "\n\n";
    ()

let rec h t bs =
    (* initialize data *)
    dataInit t bs;
    (* liveness analysis *)
    let (gens_kills, liveness) = select_type (LiveAnaly.f bs) t in
    (* (1* debug *1) Printf.printf "gens_kills\n"; *)
    (*     LiveAnaly.print gens_kills; *)
    (*     Printf.printf "liveness\n"; *)
    (*     LiveAnaly.print liveness; *)
    (*     Printf.printf "\n"; *)
        (* printData (); *)
    (* build *)
    (try
        build t bs (gens_kills, liveness)
    with Not_found -> failwith "this0");
    makeWorklist t;
    while not (break_cond ()) do
        if not (S.is_empty vertexData.simplifyWorklist) then
            try
            simplify t
            with Not_found -> failwith "this"
        else if not (MoveSet.is_empty movesData.worklistMoves) then
            try
            coalesce t
            with Not_found -> failwith "this2"
        else if not (S.is_empty vertexData.freezeWorklist) then
            try
            freeze t
            with Not_found -> failwith "this3"
        else if not (S.is_empty vertexData.spillWorklist) then
            try
            selectSpill t bs
            with Not_found -> failwith "this4"
    done;
    (try
        assignColors t
    with Not_found -> failwith "this5");
    if not (S.is_empty vertexData.spilledNodes) then
        (* (Printf.printf "spilledNodes: "; *)
        (*  S.print vertexData.spilledNodes; *)
        (*  Printf.printf "\n"; *)
        (*  let bs = rewriteProgram t bs in *)
        (*  (1* debug *1) *)
        (*      let debug = open_out "debug.txt" in *)
        (*      List.iter *)
        (*          (fun b -> Asm.printBlock debug 1 b) *)
        (*          bs; *)
        (*      close_out debug; *)
        (*  h t bs) *)
        h t (rewriteProgram t bs)
    else
        (* (printData (); *)
        (*  regAlloc bs) *)
        regAlloc bs

let rec removeBadMoves' = function
    | [] -> []
    | Instr("move", Some(x), [y], None, []) :: rem when x = y -> removeBadMoves' rem
    | Instr("mov.s", Some(x), [y], None, []) :: rem when x = y -> removeBadMoves' rem
    | e :: rem -> e :: removeBadMoves' rem
let rec removeBadMoves = function
    | [] -> []
    | b :: rem -> (removeBadMoves' b) :: (removeBadMoves rem)

let rec removeBadJumps' = function
    | [] -> []
    | Instr("j", None, [_; _], None, [l]) as i :: rem ->
            (match List.nth_opt rem 0 with
            | Some(Label(l')) when l = l' -> removeBadJumps' rem
            | _ -> i :: removeBadJumps' rem)
    | i :: rem -> i :: removeBadJumps' rem
let removeBadJumps bs =
    removeBadJumps' (List.flatten bs)

let g' bs =
    stack_size := 0;
    let bs = h Type.Int bs in
    let bs = h Type.Float bs in
    let bs = removeBadMoves bs in
    removeBadJumps bs

let g { name = Id.L(s); args = ys; fargs = zs; body_block = bs; ret = t } =
    Format.eprintf "%s - register allocating...@." s;
    g' bs

(* register allocation of the whole program *)
let f (data, funblocks, bs) =
    Format.eprintf "register allocation: may take some time (up to a few minutes, depending on the size of functions)@.";
    let funcodes = List.map g funblocks in
    Format.eprintf "Main - register allocating...@.";
    let is = g' bs in
    (data, funcodes, is)

(*
 Print function
 *)
let print chan (data, funcodes, is) =
    List.iter
        (fun is ->
            List.iter
                (fun i -> printAsm chan 0 i)
                is)
        funcodes;
    List.iter
        (fun i -> printAsm chan 0 i)
        is;
    (data, funcodes, is)
