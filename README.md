My-Min-Caml
===========

A compiler for a minimal subset of OCaml, rewritten from [min-caml](https://github.com/esumii/min-caml) for a assignment of my computer science cource.

Dependent Packages
------------------
- ocamlgraph
