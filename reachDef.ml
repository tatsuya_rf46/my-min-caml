open Ir
open CtlFlowGraph

module ExpS =
    Set.Make
        (struct
            type t = int * int (* nth block, nth exp *)
            let compare = Stdlib.compare
        end)

(* calculate def-set of a block *)
let rec calcDef i j = function
    | [] -> assert false
    | Let((x, _), _) :: rem ->
            let def, defined_vars = calcDef i (j+1) rem in
            if S.mem x defined_vars then
                def, defined_vars
            else
                ExpS.add (i, j) def, S.add x defined_vars
    | Label(_) :: rem ->
            calcDef i (j+1) rem
    | _ :: rem -> assert (rem = []); ExpS.empty, S.empty

let rec defsOfVar' i j x = function
    | [] -> assert false
    | Let((x', _), _) :: rem ->
            let defs = defsOfVar' i (j+1) x rem in
            if x = x' then ExpS.add (i, j) defs else defs
    | Label(_) :: rem ->
            defsOfVar' i (j+1) x rem
    | _ :: rem -> assert (rem = []); ExpS.empty
let rec defsOfVar i x = function
    | [] -> ExpS.empty
    | b :: rem ->
            ExpS.union (defsOfVar' i 0 x b) (defsOfVar (i+1) x rem)

(* calculate list of kills in blocks *)
let calcKills bs =
    List.fold_right2
        (fun b i rem ->
            let def, defined_vars = calcDef i 0 b in
            let defs =
                S.fold
                    (fun v rem ->
                        ExpS.union (defsOfVar i v bs) rem)
                    defined_vars
                    ExpS.empty in
            (ExpS.diff defs def) :: rem)
        bs
        (List.init (List.length bs) (fun i -> i))
        []

let array_of_set_equal x y =
    Array.for_all
        (fun x -> x)
        (Array.map2 (fun e1 e2 -> ExpS.equal e1 e2) x y)

let rec calc_lfp gens kills in_out =
    let ins = fst in_out in
    let outs = snd in_out in
    let ins' = Array.copy ins in
    let outs' = Array.copy outs in
    for i = 0 to (Array.length (fst in_out)) - 1 do
        let gen = List.nth gens i in
        let kill = List.nth kills i in
        ins.(i) <- BlockS.fold (fun i rem -> ExpS.union outs.(i) rem) (pred i) ExpS.empty;
        outs.(i) <- ExpS.union gen (ExpS.diff ins.(i) kill)
    done;
    if (array_of_set_equal ins ins') && (array_of_set_equal outs outs') then
        ()
    else
        calc_lfp gens kills in_out

let f bs =
    (* control flow graph *)
    initGraph bs;
    createIrCtlgraph bs;
    let gens = List.mapi (fun i b -> fst (calcDef i 0 b)) bs in
    let kills = calcKills bs in
    (* initialize in_out array *)
    let n = List.length bs in
    let in_out = (Array.make n ExpS.empty, Array.make n ExpS.empty) in
    (* calculate in_out *)
    calc_lfp gens kills in_out;
    in_out

let print_set l = ExpS.iter (fun (i, j) -> Printf.printf "(%s, %s) " (string_of_int i) (string_of_int j)) l

let print in_out =
    Array.iter2
        (fun livein liveout ->
            Printf.printf "in: ";
            print_set livein;
            Printf.printf "\nout: ";
            print_set liveout;
            Printf.printf "\n")
        (fst in_out) (snd in_out)
