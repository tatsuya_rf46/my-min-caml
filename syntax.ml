type pos = {line : int; chars : int}

type t =
    | Unit of pos
    | Bool of bool * pos
    | Int of int * pos
    | Float of float * pos
    | Not of t
    | Neg of t
    | Add of t * t
    | Sub of t * t
    | Mul of t      (* '<< 4' *)
    | Div of t      (* '> 2' *)
    | FNeg of t
    | FAdd of t * t
    | FSub of t * t
    | FMul of t * t
    | FDiv of t * t
    | Eq of t * t
    | LT of t * t
    | If of t * t * t
    | Let of (Id.t * Type.t) * t * t
    | Var of Id.t * pos
    | App of t * t list
    | Tuple of t list
    | LetTuple of (Id.t * Type.t) list * t * t
    | Array of t * t
    | Get of t * t
    | Put of t * t * t
and fundef = { name : (Id.t * Type.t) * pos; args : (Id.t * Type.t) list; body : t }

let rec equal = function
    | Unit(_), Unit(_) -> true
    | Int(i1, _), Int(i2, _) -> i1 = i2
    | Float(s1, _), Float(s2, _) -> s1 = s2
    | Neg(x), Neg(y) | Mul(x), Mul(y) | Div(x), Div(y) | FNeg(x), FNeg(y) -> equal(x, y)
    | Add(x, y), Add(z, w) | Sub(x, y), Sub(z, w)
    | FAdd(x, y), FAdd(z, w) | FSub(x, y), FSub(z, w)
    | FMul(x, y), FMul(z, w) | FDiv(x, y), FDiv(z, w)
    | Get(x, y), Get(z, w) -> equal(x, z) && equal(y, w)
    | Var(x, _), Var(y, _) -> x = y
    | _ -> assert false (* sabori *)

let rec printpos' e =
    match e with
    | Unit(p) | Bool(_, p) | Int(_, p) | Float(_, p) | Var(_, p) ->
            Printf.printf "line : %d; chars : %d\n" p.line p.chars
    | Not(e') | Neg(e') | Add(e', _) | Sub(e', _) | Mul(e') | Div(e')
    | FNeg e' | FAdd(e', _) | FSub(e', _) | FMul(e', _) | FDiv(e', _) | Eq(e', _)
    | LT(e', _) | If(e', _, _) | App(e', _) | Array(e', _) | Get(e', _) | Put(e', _, _)
    | Let(_, e', _) | LetTuple(_, e', _) ->
            printpos' e'
    | Tuple(ts) ->
            printpos' (List.hd ts)

let print_indent chan depth =
    for i = 1 to depth
    do
        Printf.fprintf chan "  "
    done
let rec print_args chan args depth =
    match args with
    | (id_t, type_t) :: rem ->
         print_indent chan depth;
         Printf.fprintf chan "%s\n" id_t;
         print_args chan rem depth
    | [] -> ()
let rec print_exp chan depth = function
    | Unit(_) ->
         print_indent chan depth;
         Printf.fprintf chan "UNIT\n"
    | Bool(b, _) ->
         print_indent chan depth;
         Printf.fprintf chan (if b then "BOOL true\n" else "BOOL false\n")
    | Int(i, _) ->
         print_indent chan depth;
         Printf.fprintf chan "INT %d\n" i
    | Float(s, _) ->
         print_indent chan depth;
         Printf.fprintf chan "FLOAT %f\n" s
    | Not(e) ->
         print_indent chan depth;
         Printf.fprintf chan "NOT\n";
         print_exp chan (depth+1) e
    | Neg(e) ->
         print_indent chan depth;
         Printf.fprintf chan "NEG\n";
         print_exp chan (depth+1) e
    | Add(e1, e2) ->
         print_indent chan depth;
         Printf.fprintf chan "ADD\n";
         print_exp chan (depth+1) e1;
         print_exp chan (depth+1) e2
    | Sub(e1, e2) ->
         print_indent chan depth;
         Printf.fprintf chan "SUB\n";
         print_exp chan (depth+1) e1;
         print_exp chan (depth+1) e2
    | Mul(e) ->
         print_indent chan depth;
         Printf.fprintf chan "MUL(='<< 4')\n";
         print_exp chan (depth+1) e
    | Div(e) ->
         print_indent chan depth;
         Printf.fprintf chan "DIV(='> 2')\n";
         print_exp chan (depth+1) e
    | FNeg(e) ->
         print_indent chan depth;
         Printf.fprintf chan "FNEG\n";
         print_exp chan (depth+1) e
    | FAdd(e1, e2) ->
         print_indent chan depth;
         Printf.fprintf chan "FADD\n";
         print_exp chan (depth+1) e1;
         print_exp chan (depth+1) e2
    | FSub(e1, e2) ->
         print_indent chan depth;
         Printf.fprintf chan "FSUB\n";
         print_exp chan (depth+1) e1;
         print_exp chan (depth+1) e2
    | FMul(e1, e2) ->
         print_indent chan depth;
         Printf.fprintf chan "FMUL\n";
         print_exp chan (depth+1) e1;
         print_exp chan (depth+1) e2
    | FDiv(e1, e2) ->
         print_indent chan depth;
         Printf.fprintf chan "FDIV\n";
         print_exp chan (depth+1) e1;
         print_exp chan (depth+1) e2
    | Eq(e1, e2) ->
         print_indent chan depth;
         Printf.fprintf chan "Eq\n";
         print_exp chan (depth+1) e1;
         print_exp chan (depth+1) e2
    | LT(e1, e2) ->
         print_indent chan depth;
         Printf.fprintf chan "LT\n";
         print_exp chan (depth+1) e1;
         print_exp chan (depth+1) e2
    | If(e1, e2, e3) ->
         print_indent chan depth;
         Printf.fprintf chan "IF\n";
         print_exp chan (depth+1) e1;
         print_indent chan depth;
         Printf.fprintf chan "THEN\n";
         print_exp chan (depth+1) e2;
         print_indent chan depth;
         Printf.fprintf chan "ELSE\n";
         print_exp chan (depth+1) e3
    | Let((id_t, type_t), e1, e2) ->
         print_indent chan depth;
         Printf.fprintf chan "LET %s\n" id_t;
         print_exp chan (depth+1) e1;
         print_exp chan depth e2
    | Var(id_t, _) ->
         print_indent chan depth;
         Printf.fprintf chan "VAR %s\n" id_t
    | App(e, es) ->
         print_indent chan depth;
         Printf.fprintf chan "APP\n";
         print_exp chan (depth+1) e;
         print_es chan es (depth+1)
    | Tuple(es) ->
         print_indent chan depth;
         Printf.fprintf chan "TUPLE\n";
         print_es chan es (depth+1)
    | LetTuple(args, e1, e2) ->
         print_indent chan depth;
         Printf.fprintf chan "LET\n";
         print_indent chan (depth + 1);
         Printf.fprintf chan "TUPLE\n";
         print_args chan args (depth + 2);
         print_exp chan (depth+1) e1;
         print_exp chan depth e2
    | Array(e1, e2) ->
         print_indent chan depth;
         Printf.fprintf chan "ARRAY\n";
         print_exp chan (depth+1) e1;
         print_exp chan (depth+1) e2
    | Get(e1, e2) ->
         print_indent chan depth;
         Printf.fprintf chan "GET\n";
         print_exp chan (depth+1) e1;
         print_exp chan (depth+1) e2
    | Put(e1, e2, e3) ->
         print_indent chan depth;
         Printf.fprintf chan "PUT\n";
         print_exp chan (depth+1) e1;
         print_exp chan (depth+1) e2;
         print_exp chan (depth+1) e3
and print_fundef chan f =
    let ((id_t, type_t), _) = f.name in
    Printf.fprintf chan "LETREC %s\n" id_t;
    print_indent chan 1;
    Printf.fprintf chan "ARGS\n";
    print_args chan f.args 2;
    print_indent chan 1;
    Printf.fprintf chan "BODY\n";
    print_exp chan 2 f.body
and print_es chan es depth =
    match es with
      [] -> ()
    | e :: rem -> print_exp chan depth e; print_es chan rem depth
let print chan (fundefs, e) =
    List.iter (print_fundef chan) fundefs;
    Printf.fprintf chan "MAIN\n";
    print_exp chan 1 e;
    (fundefs, e)
