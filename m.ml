(* customized version of Map *)
module M =
    Map.Make
        (struct
            type t = Id.t
            let compare = Stdlib.compare
        end)
include M

let add_list xys env = List.fold_left (fun env (x, y) -> add x y env) env xys
let add_list2 xs ys env = List.fold_left2 (fun env x y -> add x y env) env xs ys

let print m =
    M.iter (fun k _ -> Printf.printf "%s " k) m
