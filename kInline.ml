open KNormal

(* インライン展開する関数の最大サイズ (caml2html: inline_threshold) *)
let threshold = ref 100 (* Mainで-inlineオプションによりセットされる *)

let rec size = function
    | IfEq(_, _, e1, e2) | IfLT(_, _, e1, e2)
    | Let(_, e1, e2) -> 1 + size e1 + size e2
    | LetTuple(_, _, e) -> 1 + size e
    | _ -> 1

let rec h env = function (* インライン展開ルーチン本体 (caml2html: inline_g) *)
    | IfEq(x, y, e0, e1) -> IfEq(x, y, h env e0, h env e1)
    | IfLT(x, y, e0, e1) -> IfLT(x, y, h env e0, h env e1)
    | Let(xt, e0, e1) -> Let(xt, h env e0, h env e1)
    | App(x, ys) when M.mem x env -> (* 関数適用の場合 (caml2html: inline_app) *)
            let (zs, e) = M.find x env in
            Format.eprintf "inlining %s@." x;
            let env' =
            List.fold_left2
                (fun env' (z, t) y -> M.add z y env')
                M.empty
                zs
                ys in
            Alpha.g env' e
    | LetTuple(xts, y, e) -> LetTuple(xts, y, h env e)
    | e -> e

let rec g env (fundefs, e) =
    match fundefs with
    | { name = (x, t); args = yts; body = e0 } :: rem ->
            let env' = if size e0 > !threshold then env else M.add x (yts, e0) env in
            let (rem', e') = g env' (rem, e) in
            ({ name = (x, t); args = yts; body = h env' e0 } :: rem', e')
    | [] ->
            let e' = h env e in
            ([], e')

let f = g M.empty
